/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "spi.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"
#include "HIDParser.h"

/* USER CODE BEGIN Includes */
#include "usbd_hid.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
uint8_t buffer[64] = { 
	0x02,0x07,0x01,0xF1,0x7C,0x3B,0x78,0x07,0x02,0x5B,0x72,0xFF,0x7F,0x07,0x03,0x1D,
	0x72,0xC6,0x77,0x07,0x04,0x19,0x77,0xD4,0x70,0x07,0x05,0x87,0x73,0x65,0x73,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
	0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x05

};
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

HID_ReportInfo_t ParserData;
//HID_ReportItem_t *pHIDReportItemBegin;

uint8_t		TouchReportParsOk;
uint8_t		TouchDataInc						= 0xFF;
uint8_t		TouchDataStep						= 0xFF;
uint8_t		FirtTouchLen;
uint8_t 	TouchReportId						= 0xFF;
uint8_t 	TouchMaxNum							= 0xFF;
uint16_t	TouchIndexBitOffset 		= 0xFFFF;
uint8_t		TouchIndexBitSize 			= 0xFF;
uint16_t	TouchTipSwitchBitOffset = 0xFFFF;
uint8_t		TouchTipSwitchBitSize 	= 0xFF;
uint16_t	TouchXBitOffset					= 0xFFFF;
uint8_t		TouchXBitSize						= 0xFF;
uint16_t	TouchYBitOffset					= 0xFFFF;
uint16_t	TouchYBitOffset1				= 0xFFFF;
uint16_t	TouchYBitOffset2				= 0xFFFF;
uint8_t		TouchYBitSize						= 0xFF;
uint8_t		TouchYBitSize1					= 0xFF;
uint8_t		TouchTotalNum						= 0xFF;
uint16_t  TouchTotalNumBiOffset 	= 0xFFFF;
uint8_t		TouchTotalNumBitSize		= 0xFF;
uint8_t		TouchDatalen						= 0xFF;
uint16_t  TouchXLogicMax;
uint16_t  TouchYLogicMax;

uint8_t *pTipSwitch,TipSwtichMask,TipSwtichMaskRes;
uint8_t *pTouchId,TouchIdMask;
uint8_t *pTouchXLow;
uint8_t *pTouchXHi;
uint8_t *pTouchYLow;
uint8_t *pTouchYHi;

uint8_t usb_on = 1;
uint16_t KeyDownCount=0;
uint16_t KeyUpCount=0;
uint32_t time1ms;
uint32_t time10ms;
uint8_t Flag1ms=0;
uint8_t Flag10ms=0;
uint8_t g_RF24L01RxBuffer[32];
uint8_t g_RF24L01TxBuffer[32];
uint16_t i = 0;
uint16_t wLength,wLength2;
uint16_t wwLength,wwLength2;
uint8_t HIDSendReportData[64],*pHIDSendData;
uint8_t UsbNeedSend = 0;
uint8_t UsbSendLength = 0;
uint8_t UsbHaveUpdate = 0;
TouchPointInfo TouchPntOld,TouchPntNew;
uint8_t touchold_st,touchnew_st,SendDoubClick_f;
uint16_t clickpx,clickpy;

void recvNr24data(void);
void LED(void);

extern uint16_t TOUCH_SIZ_REPORT_DESC;
extern uint8_t HID_TOUCH_ReportDesc[1000];
extern uint8_t USBD_HID_Desc[USB_HID_DESC_SIZ];
extern uint8_t USBD_HID_CfgDesc[256];
extern uint8_t USBD_HID_CfgDesc_mouse[41];
extern uint8_t USBD_FS_DeviceDesc[USB_LEN_DEV_DESC];
uint8_t USB_HID_CONFIG_DESC_SIZ = 0x29;
///uint16_t  HID_MOUSE_REPORT_DESC_SIZE=0x0286;
extern uint8_t USBD_MANUFACTURER_STRING[256];
extern uint8_t  USBD_PRODUCT_STRING_FS[256];
extern uint8_t USBD_SERIALNUMBER_STRING_FS[256];
extern PCD_HandleTypeDef hpcd_USB_OTG_FS;
uint8_t flicker = 0;
uint16_t flicker_on = 0,flicker_off = 0;
#define flicker_300 150
#define flicker_500 250
#define flicker_1000 500
uint8_t flick_count = 0;
uint8_t flick_value = 0;


UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
uint8_t aRx1buf[1];
uint8_t aRx2buf[1];
uint8_t Rx1buf[Rxbufsize];
uint8_t Rx2buf[Rxbufsize];
uint16_t Rx2bufHead,Rx2bufTail,Rx2bufHeadTmp,Rx2bufTailTmp;
uint8_t Rx1count=0;
uint8_t Rx2count=0;
uint8_t Recever2OK = 0;
uint8_t Tx2buf[Rxbufsize];
uint8_t Rx2timeout=0;
uint8_t MacReportId=0xFF;

uint16_t UartLength=0;
/* USER CODE END 0 */

extern uint8_t HID_MOUSE_ReportDesc[94];

#define RIGHT_BUTTON_RANGE                20
#define RIGHT_KEY_DELAY										1500	//1500ms

extern volatile uint32_t setidle_reqed_f,MacOs_f,secdenum_f,secdenumbg_f;
extern void ReenumDevice(void);

void RightKeyProcess_MacOs(unsigned char *Pointer);
uint8_t Right_Key_Function(uint16_t x, uint16_t y);

uint8_t right_key_wait_up,right_key_press;
uint16_t start_position_x,start_position_y,right_press_time;
void recvBLEdata(void);
void MacOsDoubleClickPro();
void ParseHIDReport(void);

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	//HAL_Delay(100);
  HAL_Init();
	setidle_reqed_f = 0;
	MacOs_f = 0;
	secdenum_f = 0;
  secdenumbg_f = 0;
	Rx2bufHead=0;
	Rx2bufTail=0;
	Rx2bufHeadTmp = 0;
	Rx2bufTailTmp = 0;

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init(115200);
				Recever2OK = 0;
				Rx2count = 0;
				memset(Rx2buf,0x00,Rxbufsize);
///	HAL_Delay(500);
	
///	HAL_UART_Transmit_IT(&huart2,(uint8_t *)"@baudrate,1152#",15);
///	HAL_Delay(500);
///  MX_USART2_UART_Init(115200);
	HAL_Delay(1000);
	ParseHIDReport();
	HAL_GPIO_WritePin(GPIOC,GPIO_PIN_13,GPIO_PIN_SET);//GPIO_PIN_RESET);
  MX_USB_DEVICE_Init();
	UsbHaveUpdate = 1;
#if 0
	printf("NRF24L01 2.4G\n");
	while(NRF24L01_Check())
	{
		printf("1\r\n"); 
		HAL_Delay(1000);
	}
  printf("0");
  NRF24L01_RX_Mode();
  printf("input rx mode \n");
#endif	
	time1ms = HAL_GetTick();//synchronised time
	touchold_st = 0;
	TouchPntNew.TouchDnTm = 0xffff;
	TouchPntNew.TouchUpTm = 0xffff;
	TouchPntNew.lasttimer = time1ms;
	SendDoubClick_f = 0;
  /* USER CODE BEGIN 2 */
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

				//recvNr24data();
			//if(Recever2OK>0)
			//{
				recvBLEdata();
			//	Recever2OK = 0;
			//	Rx2count = 0;
			//	memset(Rx2buf,0x00,Rxbufsize);
			//}
  /* USER CODE BEGIN 3 */
		if(HAL_GetTick() - time1ms > 1)//Judgment time difference1ms
		{
			Flag1ms = 1;//1ms Time base
			time1ms = HAL_GetTick();//synchronised time
			if(time10ms>9)
			{
				time10ms = 0;
				Flag10ms = 1;//10ms Time base
			}
			else
			{
				time10ms++;
			}
			
///			if(Rx2timeout<10)
	///		{
///				Rx2timeout++;
///			}
		}
		
		if(Flag1ms)
		{
			Flag1ms = 0;
			if(HAL_GPIO_ReadPin(KEY_GPIO_Port,KEY_Pin)==0)
			{
				//key1 down
				if(KeyDownCount>2500)//after 5sec 
				{
					#if 0
					if(usb_on!=2)
					{
						usb_on = 2;//強制進入配對中
						flicker = 1;
						flicker_on = 0;
						flicker_off = 0;
					}
					#endif
				}
				else if(KeyDownCount>20)	//after 20ms debounce
				{
					KeyDownCount++;	
					//USBD_HID_SendReport(&hUsbDeviceFS, buffer,62);
					KeyUpCount = 0;
				}
				else
				{
					KeyDownCount++;
				}
			}
			else
			{
				//key1 up
				if(KeyUpCount>20)	//after 20ms debounce
				{
					if(KeyDownCount>2500)
					{
						//進入配對
					}
					else if(KeyDownCount<5000 &&KeyDownCount>20)
					{
						if(usb_on==0)	//關機中?
						{
							usb_on=1;
							HAL_GPIO_WritePin(GPIOC,GPIO_PIN_13,GPIO_PIN_SET);//GPIO_PIN_RESET);
							MX_USB_DEVICE_Init();
							USBD_Start(&hUsbDeviceFS);
							#if 0
							if(UsbHaveUpdate>0)
							{
								//有配對過
								usb_on=1;
								HAL_GPIO_WritePin(GPIOC,GPIO_PIN_13,GPIO_PIN_SET);//GPIO_PIN_RESET);
								MX_USB_DEVICE_Init();
								USBD_Start(&hUsbDeviceFS);
							}
							else
							{
								//沒有配對過
								usb_on = 3;//未配對閃爍提�
								flicker = 1;
								flicker_on = 0,
								flicker_off = 0;
								flick_value = 3;
								flick_count = 0;
							}
							#endif
						}
						else
						{
							usb_on=0;
							HAL_GPIO_WritePin(GPIOC,GPIO_PIN_13,GPIO_PIN_RESET);//GPIO_PIN_SET);
							USBD_LL_DevDisconnected(&hUsbDeviceFS);
						}
					}
					KeyDownCount = 0;
				}
				else
				{
					KeyUpCount++;
				}
			}
			LED();//燈號處理
		}
		
		if(secdenumbg_f)
		{
			secdenumbg_f = 0;
			ReenumDevice();
		}
	}
		/* USER CODE END 3 */
}

void RightKeyProcess_MacOs(unsigned char *Pointer)
{
	uint16_t X,Y;

 if(*(Pointer+1) == 0x01)
 {
	if(right_key_wait_up)
		*(Pointer+1) = 0x00;
	else
	{
	    X = (uint16_t)(*(Pointer+3))*256 + (*(Pointer+2));
  	  Y = (uint16_t)(*(Pointer+5))*256 + (*(Pointer+4));	
	  if( Right_Key_Function((uint16_t)X,(uint16_t)Y) )
  		*(Pointer+1) = 0x02;
	}
 }
  else
  {
		right_key_press=0;
		right_key_wait_up=0;
  }
}

void LED(void)
{
	//燈號處理每1ms處理一次
	switch(usb_on)
	{
		case 0:
			//關機
			HAL_GPIO_WritePin(LED_GPIO_Port,LED_Pin1,GPIO_PIN_SET);
			HAL_GPIO_WritePin(LED_GPIO_Port,LED_Pin2,GPIO_PIN_RESET);
			break;
		case 1:
			//開機
			HAL_GPIO_WritePin(LED_GPIO_Port,LED_Pin1,GPIO_PIN_RESET);
			HAL_GPIO_WritePin(LED_GPIO_Port,LED_Pin2,GPIO_PIN_SET);
			break;
		case 2:
			HAL_GPIO_WritePin(LED_GPIO_Port,LED_Pin1,GPIO_PIN_RESET);//藍燈滅
			if(flicker>0)
			{
				//亮
				if(flicker_on>flicker_500)
				{
					//超過時間
					flicker = 0;//切滅
					flicker_off = 0;
					HAL_GPIO_WritePin(LED_GPIO_Port,LED_Pin2,GPIO_PIN_RESET);//綠燈滅
					if(flick_count>flick_value)
					{
						//閃爍						
						usb_on = 0;//進入關機
					}
				}
				else
				{
					flicker_on++;
				}
			}
			else
			{
				//滅
				if(flicker_off>flicker_500)
				{
					//超過時間
					flicker = 1;//切亮
					flicker_on = 0;
					HAL_GPIO_WritePin(LED_GPIO_Port,LED_Pin2,GPIO_PIN_SET);//綠燈亮
				}
				else
				{
					flicker_off++;
				}
			}
			//配對中
			break;
		case 3:
			//未配對提示
			HAL_GPIO_WritePin(LED_GPIO_Port,LED_Pin2,GPIO_PIN_RESET);//綠燈滅
			if(flicker>0)
			{
				//亮
				if(flicker_on>flicker_300)
				{
					//超過時間
					flicker = 0;//切滅
					flicker_off = 0;
					HAL_GPIO_WritePin(LED_GPIO_Port,LED_Pin1,GPIO_PIN_RESET);//藍燈滅
					if(flick_count>flick_value)
					{
						usb_on = 0;
					}
					else
					{
						flick_count++;
					}
				}
				else
				{
					flicker_on++;
				}
			}
			else
			{
				//滅
				if(flicker_off>flicker_300)
				{
					//超過時間
					flicker = 1;//切亮
					flicker_on = 0;
					HAL_GPIO_WritePin(LED_GPIO_Port,LED_Pin1,GPIO_PIN_SET);//藍燈亮
				}
				else
				{
					flicker_off++;
				}
			}
			break;
	}
}

void recvBLEdata(void)
{
	uint8_t analysis[9];
	uint8_t s1=0;
	uint16_t rx2bufhead_tmp,Rx2bufTail_tmp;
	uint16_t rx2datalen;
	uint8_t rx2cmd,uartlen1,uartlen2,rx2head;
	
	rx2bufhead_tmp = Rx2bufHead;
	Rx2bufTail_tmp = Rx2bufTail;
	if(rx2bufhead_tmp==Rx2bufTail_tmp) return;
	
	if(rx2bufhead_tmp<Rx2bufTail_tmp)
		rx2datalen = (Rxbufsize-Rx2bufTail_tmp)+rx2bufhead_tmp;
	else
	  rx2datalen = (rx2bufhead_tmp-Rx2bufTail_tmp);
	
	if(rx2datalen<6)
	{
			//Rx2bufTail = rx2bufhead_tmp;
			return;
	}
	rx2head = Rx2buf[Rx2bufTail_tmp++];
	Rx2bufTail_tmp &= RXBUFSIZEMASK;
	if(rx2head != 0xF1)
	{
		Rx2bufTail = Rx2bufTail_tmp;
		return;
	}
	rx2head = Rx2buf[Rx2bufTail_tmp++];
	Rx2bufTail_tmp &= RXBUFSIZEMASK;
	if(rx2head != 0xA5)
	{
			Rx2bufTail++;
			Rx2bufTail &= RXBUFSIZEMASK;
			return;
	}
	rx2head = Rx2buf[Rx2bufTail_tmp++];
	Rx2bufTail_tmp &= RXBUFSIZEMASK;
	if(rx2head != 0x5A)
	{
			Rx2bufTail++;
			Rx2bufTail &= RXBUFSIZEMASK;
			return;
	}
	uartlen1 = Rx2buf[Rx2bufTail_tmp++];
	Rx2bufTail_tmp &= RXBUFSIZEMASK;
	uartlen2 = Rx2buf[Rx2bufTail_tmp++];
	Rx2bufTail_tmp &= RXBUFSIZEMASK;	
	UartLength = (((uint16_t)uartlen1)<<8)+uartlen2;
	if(rx2datalen<(UartLength+5)) return;
	
	rx2cmd   = Rx2buf[Rx2bufTail_tmp++];
	Rx2bufTail_tmp &= RXBUFSIZEMASK;
	
	switch(rx2cmd)
	{
		case 0xA0:
					if(UartLength !=3)
					{
						Rx2bufTail+=3;
						Rx2bufTail &= RXBUFSIZEMASK;
						break;
					}
					if(usb_on!=2)
					{
						usb_on = 2;//強制進入配對中
						flicker = 1;
						flicker_on = 0;
						flicker_off = 0;
					}
					USBD_HID_CfgDesc[26] = Rx2buf[Rx2bufTail_tmp++];
					Rx2bufTail_tmp &= RXBUFSIZEMASK;
					USBD_HID_CfgDesc[25] = Rx2buf[Rx2bufTail_tmp++];
					Rx2bufTail_tmp &= RXBUFSIZEMASK;	
					USBD_HID_Desc[8] = USBD_HID_CfgDesc[26];//Rx2buf[1];
					USBD_HID_Desc[7] = USBD_HID_CfgDesc[25];//Rx2buf[2];
					TOUCH_SIZ_REPORT_DESC = ((uint16_t)USBD_HID_Desc[8]<<8)+USBD_HID_Desc[7];		
					Rx2bufTail = Rx2bufTail_tmp;
			break;
		/*case 0xA1:
			
			memset(USBD_HID_CfgDesc,0x00,255);
		  for(i=0;i<UartLength;i++)
			{
				USBD_HID_CfgDesc[i] = Rx2buf[Rx2bufTail++];
				Rx2bufTail &= RXBUFSIZEMASK;//Rx2buf[i+3];
			}
			while(UartLength>s1)
			{
				if(USBD_HID_CfgDesc[s1]==0x09)
				{
					for(i=0;i<9;i++)
					{
						analysis[i] = USBD_HID_CfgDesc[s1++];
					}
					if(analysis[1]==0x21)
					{
						for(i=0;i<9;i++)
						{
							USBD_HID_Desc[i] = analysis[i];
							s1 = UartLength;
						}
					}
					else
					{
						s1 = s1+9;
					}
					
				}
				else if(USBD_HID_CfgDesc[s1]==0x07)
				{
					s1 = s1+7;
				}
				else
				{
					s1 = UartLength;
				}
				TOUCH_SIZ_REPORT_DESC = ((uint16_t)USBD_HID_Desc[8]<<8)+USBD_HID_Desc[7];
			}
			break;
		case 0xA2:
			memset(USBD_MANUFACTURER_STRING,0x00,255);
		  for(i=0;i<UartLength;i++)
			{
				USBD_MANUFACTURER_STRING[i] = Rx2buf[Rx2bufTail++];
				Rx2bufTail &= RXBUFSIZEMASK;//Rx2buf[i+3];
			}
			break;
		case 0xA3:
			memset(USBD_PRODUCT_STRING_FS,0x00,255);
		  for(i=0;i<UartLength;i++)
			{
				USBD_PRODUCT_STRING_FS[i] = Rx2buf[Rx2bufTail++];
				Rx2bufTail &= RXBUFSIZEMASK;//Rx2buf[i+3];
			}
			break;
		case 0xA4:
			memset(USBD_SERIALNUMBER_STRING_FS,0x00,255);
		  for(i=0;i<UartLength;i++)
			{
				USBD_SERIALNUMBER_STRING_FS[i] = Rx2buf[Rx2bufTail++];
				Rx2bufTail &= RXBUFSIZEMASK;//Rx2buf[i+3];
			}
			break;*/
		case 0xA5:
			UartLength -= 1;
			if(UartLength !=200)
			{
				Rx2bufTail+=3;
				Rx2bufTail &= RXBUFSIZEMASK;
				break;
			}
			for(i=0;i<200;i++)
			{
				HID_TOUCH_ReportDesc[i] = Rx2buf[Rx2bufTail_tmp++];
				Rx2bufTail_tmp &= RXBUFSIZEMASK;//Rx2buf[i+3];
			}
			Rx2bufTail = Rx2bufTail_tmp;
			usb_on = 0;
			break;
		case 0xA6:
			UartLength -= 1;
			if(UartLength !=200)
			{
				Rx2bufTail+=3;
				Rx2bufTail &= RXBUFSIZEMASK;
				break;
			}
			for(i=0;i<200;i++)
			{
				HID_TOUCH_ReportDesc[i+200] = Rx2buf[Rx2bufTail_tmp++];
				Rx2bufTail_tmp &= RXBUFSIZEMASK;//Rx2buf[i+3];
			}
			Rx2bufTail = Rx2bufTail_tmp;
			break;
		case 0xA7:
			UartLength -= 1;
			if(UartLength !=200)
			{
				Rx2bufTail+=3;
				Rx2bufTail &= RXBUFSIZEMASK;
				break;
			}
			for(i=0;i<200;i++)
			{
				HID_TOUCH_ReportDesc[i+400] = Rx2buf[Rx2bufTail_tmp++];
				Rx2bufTail_tmp &= RXBUFSIZEMASK;//Rx2buf[i+3];
			}
			Rx2bufTail = Rx2bufTail_tmp;
			break;
		case 0xA8:
			UartLength -= 1;
			if(UartLength !=200)
			{
				Rx2bufTail+=3;
				Rx2bufTail &= RXBUFSIZEMASK;
				break;
			}
			for(i=0;i<200;i++)
			{
				HID_TOUCH_ReportDesc[i+600] = Rx2buf[Rx2bufTail_tmp++];
				Rx2bufTail_tmp &= RXBUFSIZEMASK;//Rx2buf[i+3];
			}
			Rx2bufTail = Rx2bufTail_tmp;
			break;
			//
		case 0xA9:
			UartLength -= 1;
			if(UartLength !=200)
			{
				Rx2bufTail+=3;
				Rx2bufTail &= RXBUFSIZEMASK;
				break;
			}
			for(i=0;i<200;i++)
			{
				HID_TOUCH_ReportDesc[i+800] = Rx2buf[Rx2bufTail_tmp++];
				Rx2bufTail_tmp &= RXBUFSIZEMASK;//Rx2buf[i+3];
			}
			Rx2bufTail = Rx2bufTail_tmp;
			usb_on = 1;
			//HAL_GPIO_WritePin(GPIOB,GPIO_PIN_12,GPIO_PIN_SET);//GPIO_PIN_RESET);
			HAL_GPIO_WritePin(GPIOC,GPIO_PIN_13,GPIO_PIN_RESET);
			USBD_LL_DevDisconnected(&hUsbDeviceFS);				
			MX_USB_DEVICE_Init();
			if(MacOs_f==1)
				secdenum_f = 1;
			USBD_Start(&hUsbDeviceFS);
			//HAL_Delay(10000);
			HAL_GPIO_WritePin(GPIOC,GPIO_PIN_13,GPIO_PIN_SET);//GPIO_PIN_RESET);
			ParseHIDReport();
			MacReportId = 0xFF;
			UsbHaveUpdate = 0;
			break;
		case 0xB1:
					if(UartLength !=5)
					{
						Rx2bufTail+=3;
						Rx2bufTail &= RXBUFSIZEMASK;
						break;
					}
					Rx2bufTail = Rx2bufTail_tmp;
					if(Rx2buf[Rx2bufTail_tmp++]!=0xa5)
					{
							break;
					}
					Rx2bufTail_tmp &= RXBUFSIZEMASK;
					if(Rx2buf[Rx2bufTail_tmp++]!=0x5a)
					{
							break;
					}
					Rx2bufTail_tmp &= RXBUFSIZEMASK;
					if(Rx2buf[Rx2bufTail_tmp++]!=0xaa)
					{
							break;
					}
					Rx2bufTail_tmp &= RXBUFSIZEMASK;
					if(Rx2buf[Rx2bufTail_tmp++]!=0x55)
					{
							break;
					}
					Rx2bufTail_tmp &= RXBUFSIZEMASK;
					Rx2bufTail = Rx2bufTail_tmp;

					if(TouchReportParsOk==0) break;
					//if(HIDSendReportData[0]!=TouchReportId) break;
					if(TouchDatalen!=0xFF)
					{
						uint8_t *pTipSwitchTmp;
						
						HIDSendReportData[0] = TouchReportId;
						pTipSwitchTmp = pTipSwitch;
						for(i=FirtTouchLen;i<TouchDatalen;)
						{
							//HIDSendReportData[i] = 0;
							*pTipSwitchTmp &= TipSwtichMaskRes;
							pTipSwitchTmp+=TouchDataStep;
							i+=TouchDataStep;
						}
						goto sendtouchdata ;
					}
			break;
		case 0xB0:
			UartLength -= 1;
			if(UartLength >64)
			{
				Rx2bufTail+=3;
				Rx2bufTail &= RXBUFSIZEMASK;
				break;
			}
			for(i=0;i<UartLength;i++)
			{
				HIDSendReportData[i] = Rx2buf[Rx2bufTail_tmp++];
				Rx2bufTail_tmp &= RXBUFSIZEMASK;//				Rx2buf[i+3];
			}
			Rx2bufTail = Rx2bufTail_tmp;
			if(TouchReportParsOk==0) break;
			//if(HIDSendReportData[0]!=TouchReportId) break;
			if(TouchDatalen==0xFF)
				TouchDatalen = UartLength;
			//if(HIDSendReportData[61]!=1) 
			//	UartLength += 2;
sendtouchdata:			
			if(MacOs_f==1)
			{
				buffer[0] = 0x00;
				if(MacReportId==0xFF)
				{
					if((*pTipSwitch & TipSwtichMask))
					{
							buffer[0] = 0x01;
							buffer[1] = 0x01;
							buffer[2] = *pTouchXLow;					//??X?8?
							buffer[3] = *pTouchXHi;					//??X?8?
							buffer[4] = *pTouchYLow;					//??Y?8?
							buffer[5] = *pTouchYHi;					//??Y?8?
							MacReportId = (*pTouchId & TouchIdMask);
					}
				}
				else
				{
					buffer[0] = 0x01;
					if(MacReportId != (*pTouchId & TouchIdMask))
					{
							buffer[1] = 0x00;
					}
					else
					{
						if((*pTipSwitch & TipSwtichMask))
						{
								buffer[1] = 0x01;
						}
						else
						{
								buffer[1] = 0x00;
						}
						buffer[2] = *pTouchXLow;					//??X?8?
						buffer[3] = *pTouchXHi;					//??X?8?
						buffer[4] = *pTouchYLow;					//??Y?8?
						buffer[5] = *pTouchYHi;					//??Y?8?
					}
				}	
				if(buffer[0]==1)
				{
					MacOsDoubleClickPro();
					RightKeyProcess_MacOs(buffer);
					if(buffer[1]==0x02)
						buffer[6] = 0;
					else
						buffer[6] = buffer[1];
						buffer[7] = 0;
						
					if(usb_on==1)
					{
						USBD_HID_SendReport(&hUsbDeviceFS,buffer,8);
						if(SendDoubClick_f)
						{
									HAL_Delay(3);
									buffer[1] = 0x01;
									USBD_HID_SendReport(&hUsbDeviceFS,buffer,8);
									HAL_Delay(3);
									buffer[1] = 0x00;
									USBD_HID_SendReport(&hUsbDeviceFS,buffer,8);
						}
					}
				}
			}
			else
			{
				if(usb_on==1)
				{
					USBD_HID_SendReport(&hUsbDeviceFS, HIDSendReportData,TouchDatalen);//UartLength);	
				}
			}
			break;
//		case 0xB1:	
//			UartLength -= 1;
//			if(UartLength >64)
//			{
//				Rx2bufTail+=3;
//				Rx2bufTail &= RXBUFSIZEMASK;
//				break;
//			}
//			for(i=0;i<UartLength;i++)
//			{
//				HIDSendReportData[i] = Rx2buf[Rx2bufTail_tmp++];
//				Rx2bufTail_tmp &= RXBUFSIZEMASK;//Rx2buf[i+3];
//			}
//			Rx2bufTail = Rx2bufTail_tmp;
//			if(MacOs_f==1)
//			{
//				/*
//					buffer[0] = 0x01;
//					buffer[1] = 0x01;					// ==0x01表示左鍵按下, ==0x00表示左鍵抬起
//					buffer[2] = 0x12;					//座標X低8位
//					buffer[3] = 0x03;					//座標X高8位
//					buffer[4] = 0x12;					//座標Y低8位
//					buffer[5] = 0x03;					//座標Y高8位
//						
//					RightKeyProcess_MacOs(buffer);
//					if(buffer[1]==0x02)
//						buffer[6] = 0;
//					else
//						buffer[6] = buffer[1];
//					buffer[7] = 0;		  
//				*/
//				buffer[0] = 0x01;
//						
//				if(HIDSendReportData[1] == 0x04)
//				{
//					buffer[1] = 0x00;					// ==0x01表示左鍵按下, ==0x00表示左鍵抬起
//				}
//				else if(HIDSendReportData[1] == 0x07)
//				{
//					buffer[1] = 0x01;					// ==0x01表示左鍵按下, ==0x00表示左鍵抬起
//				}
//				else
//				{
//					buffer[1] = 0x00;					// ==0x01表示左鍵按下, ==0x00表示左鍵抬起
//				}
//				buffer[2] = HIDSendReportData[2];					//座標X低8位
//				buffer[3] = HIDSendReportData[3];					//座標X高8位
//				buffer[4] = HIDSendReportData[4];					//座標Y低8位
//				buffer[5] = HIDSendReportData[5];					//座標Y高8位
//						
//				MacOsDoubleClickPro();
//				RightKeyProcess_MacOs(buffer);
//				if(buffer[1]==0x02)
//					buffer[6] = 0;
//				else
//					buffer[6] = buffer[1];
//					buffer[7] = 0;
//					
//				if(usb_on==1)
//				{
//					USBD_HID_SendReport(&hUsbDeviceFS,buffer,8);
//					if(SendDoubClick_f)
//					{
//								HAL_Delay(3);
//								buffer[1] = 0x01;
//								USBD_HID_SendReport(&hUsbDeviceFS,buffer,8);
//								HAL_Delay(3);
//								buffer[1] = 0x00;
//								USBD_HID_SendReport(&hUsbDeviceFS,buffer,8);
//					}
//				}
//			}
//			else
//			{
//				if(usb_on==1)
//				{
//					USBD_HID_SendReport(&hUsbDeviceFS, HIDSendReportData,64);	
//				}
//			}
//			break;
			
		default:
			Rx2bufTail+=3;
			Rx2bufTail &= RXBUFSIZEMASK;	
			break;
	}
	//Rx2bufTail += 2;
	//Rx2bufTail &= RXBUFSIZEMASK;
}

void MacOsDoubleClickPro()
{
	uint32_t timergettmp,lastclickuptime,lastclickdntime;
	uint16_t PntDistX,PntDistY,clickpxtmp,clickpytmp;
	
	timergettmp = HAL_GetTick();
	if(buffer[1]==1)
		touchnew_st = 1;
	else
		touchnew_st = 0;
	SendDoubClick_f=0;
	if(touchnew_st)
	{
			if(touchold_st == 0)
			{
					clickpx = (buffer[3]*256)+buffer[2];
				  clickpy = (buffer[5]*256)+buffer[4];
					lastclickuptime = TouchPntNew.lasttimer;
					if(timergettmp>lastclickuptime)
							TouchPntNew.TouchUpTm = timergettmp-lastclickuptime;
					else
							TouchPntNew.TouchUpTm = 0xffff;
					TouchPntOld.TouchDnTm=TouchPntNew.TouchDnTm;
					TouchPntOld.TouchUpTm=TouchPntNew.TouchUpTm;
					TouchPntOld.DnX=TouchPntNew.DnX;
					TouchPntOld.DnY=TouchPntNew.DnY;
				  TouchPntOld.MaxDiff=TouchPntNew.MaxDiff;
					
					TouchPntNew.lasttimer = timergettmp;
					TouchPntNew.DnX = clickpx;
					TouchPntNew.DnY = clickpy;
					TouchPntNew.MaxDiff = 0;
					if((TouchPntOld.TouchDnTm<500)&&(TouchPntOld.TouchUpTm<500)&&(TouchPntOld.MaxDiff<SINGLE_TOUCH_MOVE_MAX_DIST))
					{
							if(clickpx>TouchPntOld.DnX)
								PntDistX = clickpx-TouchPntOld.DnX;
							else
								PntDistX = TouchPntOld.DnX-clickpx;
							if(clickpy>TouchPntOld.DnY)
								PntDistY = clickpy-TouchPntOld.DnY;
							else
								PntDistY = TouchPntOld.DnY-clickpy;
						
						if((PntDistX<DOUBLE_TOUCH_MAX_DIST)&&(PntDistY<DOUBLE_TOUCH_MAX_DIST))
						{
	//					  	if(PntDist<SINGLE_TOUCH_MAX_DIST)
	//						{
								clickpx = (clickpx+(TouchPntOld.DnX*9))/10;
								clickpy = (clickpy+(TouchPntOld.DnY*9))/10;
								buffer[2]=(uint8_t)clickpx;
								buffer[3]=(uint8_t)(clickpx>>8);
								buffer[4]=(uint8_t)clickpy;
								buffer[5]=(uint8_t)(clickpy>>8);
	//						}					  
						}
					}
			}
			else
			{
					lastclickdntime = TouchPntNew.lasttimer;
					if(timergettmp>lastclickdntime)
							TouchPntNew.TouchDnTm = timergettmp-lastclickdntime;
					else
							TouchPntNew.TouchDnTm = 0xffff;
					
					if((TouchPntNew.TouchDnTm<500)&&(TouchPntNew.MaxDiff<SINGLE_TOUCH_MOVE_MAX_DIST))
					{
							clickpxtmp = (buffer[3]*256)+buffer[2];
							clickpytmp = (buffer[5]*256)+buffer[4];
						
							if(clickpxtmp>TouchPntNew.DnX)
								PntDistX = clickpxtmp-TouchPntNew.DnX;
							else
								PntDistX = TouchPntNew.DnX-clickpxtmp;
							if(PntDistX>TouchPntNew.MaxDiff)
								TouchPntNew.MaxDiff = PntDistX;
							if(clickpytmp>TouchPntNew.DnY)
								PntDistY = clickpytmp-TouchPntNew.DnY;
							else
								PntDistY = TouchPntNew.DnY-clickpytmp;
							if(PntDistY>TouchPntNew.MaxDiff)
								TouchPntNew.MaxDiff = PntDistY;	
							if(TouchPntNew.MaxDiff<SINGLE_TOUCH_MOVE_MAX_DIST)
							{
								buffer[2]=(uint8_t)clickpx;
								buffer[3]=(uint8_t)(clickpx>>8);
								buffer[4]=(uint8_t)clickpy;
								buffer[5]=(uint8_t)(clickpy>>8);						  
							}
						}
			}
	}
	else
	{
			if(touchold_st)
			{
					lastclickdntime = TouchPntNew.lasttimer;
					if(timergettmp>lastclickdntime)
							TouchPntNew.TouchDnTm = timergettmp-lastclickdntime;
					else
							TouchPntNew.TouchDnTm = 0xffff;
					
					if((TouchPntNew.TouchDnTm<500)&&(TouchPntNew.MaxDiff<SINGLE_TOUCH_MOVE_MAX_DIST))
					{
								buffer[2]=(uint8_t)clickpx;
								buffer[3]=(uint8_t)(clickpx>>8);
								buffer[4]=(uint8_t)clickpy;
								buffer[5]=(uint8_t)(clickpy>>8);		
								SendDoubClick_f = 1;
						}
						TouchPntNew.lasttimer = timergettmp;
			}
	}
	touchold_st = touchnew_st;
}


//void recvNr24data(void)
//{

//			i = NRF24L01_RxPacket(g_RF24L01RxBuffer);
//			if(i==0)
//			{
//				wLength2 = 0;
//				if((g_RF24L01RxBuffer[0]>0xB0)&&(g_RF24L01RxBuffer[0]<0xBB))
//				{
//					HIDSendReportData[61] = g_RF24L01RxBuffer[0]&0x0F;
//					if(HIDSendReportData[61]<0x0B)
//					{
//						for(i=1;i<32;i++)
//						{
//							HIDSendReportData[wLength2++] = g_RF24L01RxBuffer[i];
//						}
//						if(HIDSendReportData[61]>5)
//						{
//							HIDSendReportData[61] = 5;
//						}
//						USBD_HID_SendReport(&hUsbDeviceFS, HIDSendReportData,62);	
//					}
//				}
//				else if(g_RF24L01RxBuffer[0]==0xBB)
//				{				
//					wLength = g_RF24L01RxBuffer[1];
//					UsbSendLength = wLength;
//					
//					for(i=2;i<32;i++)
//					{
//						HIDSendReportData[wLength2++] = g_RF24L01RxBuffer[i];
//						if(--wLength==0)
//						{
//							i=32;
//						}
//					}
//					while(wLength!=0)
//					{
//						while(NRF24L01_RxPacket(g_RF24L01RxBuffer));
//						for(i=0;i<32;i++)
//						{
//							HIDSendReportData[wLength2++] = g_RF24L01RxBuffer[i];
//							if(--wLength==0)
//							{
//								i=32;
//							}
//						}
//					}
//				
//				if(MacOs_f==1)
//				{
//					/*
//						buffer[0] = 0x01;
//						buffer[1] = 0x01;					// ==0x01表示左鍵按下, ==0x00表示左鍵抬起
//						buffer[2] = 0x12;					//座標X低8位
//						buffer[3] = 0x03;					//座標X高8位
//						buffer[4] = 0x12;					//座標Y低8位
//						buffer[5] = 0x03;					//座標Y高8位
//						
//						RightKeyProcess_MacOs(buffer);
//						if(buffer[1]==0x02)
//							buffer[6] = 0;
//						else
//							buffer[6] = buffer[1];
//						buffer[7] = 0;		  
//					*/
//						buffer[0] = 0x01;
//						
//						if(HIDSendReportData[1] == 0x04)
//						{
//							buffer[1] = 0x00;					// ==0x01表示左鍵按下, ==0x00表示左鍵抬起
//						}
//						else if(HIDSendReportData[1] == 0x07)
//						{
//							buffer[1] = 0x01;					// ==0x01表示左鍵按下, ==0x00表示左鍵抬起
//						}
//						else
//						{
//							buffer[1] = 0x00;					// ==0x01表示左鍵按下, ==0x00表示左鍵抬起
//						}
//						buffer[2] = HIDSendReportData[3];					//座標X低8位
//						buffer[3] = HIDSendReportData[4];					//座標X高8位
//						buffer[4] = HIDSendReportData[5];					//座標Y低8位
//						buffer[5] = HIDSendReportData[6];					//座標Y高8位
//						
//						RightKeyProcess_MacOs(buffer);
//						if(buffer[1]==0x02)
//							buffer[6] = 0;
//						else
//							buffer[6] = buffer[1];
//						buffer[7] = 0;
//					
//						USBD_HID_SendReport(&hUsbDeviceFS,buffer,8);
//				}
//				else
//				{
//					//USBD_HID_SendReport(&hUsbDeviceFS,buffer, 4);
//					USBD_HID_SendReport(&hUsbDeviceFS, HIDSendReportData,wLength2);	
//				}
//				
//				
//				
//				}
//				else if((g_RF24L01RxBuffer[0]==0xAA)&&(g_RF24L01RxBuffer[1]==0xAA))
//				{
//					while(NRF24L01_RxPacket(g_RF24L01RxBuffer));//????????
//					wLength2 = 0;
//					//printf( "Dev_Desc:\r\n");
//					for(i=3;i<21;i++)
//					{
//						USBD_FS_DeviceDesc[wLength2++] = g_RF24L01RxBuffer[i];
//						//printf("0x%02X " ,(uint8_t)g_RF24L01RxBuffer[i]);
//					}
//					while(NRF24L01_RxPacket(g_RF24L01RxBuffer));//????????
//					wLength2 = 0;
//					//printf( "\r\nConf_Desc:\r\n");
//					wLength = ((uint16_t)g_RF24L01RxBuffer[1]<<8)+g_RF24L01RxBuffer[2];
//					USB_HID_CONFIG_DESC_SIZ = (uint8_t)wLength;
//					for(i=3;i<32;i++)
//					{
//						//printf("0x%02X " ,(uint8_t)g_RF24L01RxBuffer[i]);
//						USBD_HID_CfgDesc[wLength2++] = g_RF24L01RxBuffer[i];
//						if(--wLength==0)
//						{
//							i=32;
//						}
//					}
//					while(wLength!=0)
//					{
//						while(NRF24L01_RxPacket(g_RF24L01RxBuffer));
//						for(i=0;i<32;i++)
//						{
//							//printf("0x%02X " ,(uint8_t)g_RF24L01RxBuffer[i]);
//							USBD_HID_CfgDesc[wLength2++] = g_RF24L01RxBuffer[i];
//							if(--wLength==0)
//							{
//								i=32;
//							}
//						}
//					}
//					uint8_t analysis[9];
//					uint8_t s1=0;
//					while(wLength2>s1)
//					{
//						if(USBD_HID_CfgDesc[s1]==0x09)
//						{
//							for(i=0;i<9;i++)
//							{
//								analysis[i] = USBD_HID_CfgDesc[s1++];
//							}
//							if(analysis[1]==0x21)
//							{
//								for(i=0;i<9;i++)
//								{
//									USBD_HID_Desc[i] = analysis[i];
//									s1 = wLength2;
//								}
//							}
//						}
//						else if(USBD_HID_CfgDesc[s1]==0x07)
//						{
//							s1 = s1+7;
//						}
//					}
//					while(NRF24L01_RxPacket(g_RF24L01RxBuffer));//????????
//					wLength2 = 0;
//					//printf( "\r\nManuFacturerStr:\r\n");
//					wLength = ((uint16_t)g_RF24L01RxBuffer[1]<<8)+g_RF24L01RxBuffer[2];
//					for(i=3;i<32;i++)
//					{
//						//printf("0x%02X " ,(uint8_t)g_RF24L01RxBuffer[i]);
//						USBD_MANUFACTURER_STRING[wLength2++] = g_RF24L01RxBuffer[i];
//						if(--wLength==0)
//						{
//							i=32;
//						}
//					}
//					while(wLength!=0)
//					{
//						while(NRF24L01_RxPacket(g_RF24L01RxBuffer));
//						for(i=0;i<32;i++)
//						{
//							//printf("0x%02X " ,(uint8_t)g_RF24L01RxBuffer[i]);
//							USBD_MANUFACTURER_STRING[wLength2++] = g_RF24L01RxBuffer[i];
//							if(--wLength==0)
//							{
//								i=32;
//							}
//						}
//					}
//					while(NRF24L01_RxPacket(g_RF24L01RxBuffer));//????????
//					wLength2 = 0;
//					//printf( "\r\nProductStr:\r\n");
//					wLength = ((uint16_t)g_RF24L01RxBuffer[1]<<8)+g_RF24L01RxBuffer[2];
//					for(i=3;i<32;i++)
//					{
//						//printf("0x%02X " ,(uint8_t)g_RF24L01RxBuffer[i]);
//						USBD_PRODUCT_STRING_FS[wLength2++] = g_RF24L01RxBuffer[i];
//						if(--wLength==0)
//						{
//							i=32;
//						}
//					}
//					while(wLength!=0)
//					{
//						while(NRF24L01_RxPacket(g_RF24L01RxBuffer));
//						for(i=0;i<32;i++)
//						{
//							//printf("0x%02X " ,(uint8_t)g_RF24L01RxBuffer[i]);
//							USBD_PRODUCT_STRING_FS[wLength2++] = g_RF24L01RxBuffer[i];
//							if(--wLength==0)
//							{
//								i=32;
//							}
//						}
//					}
//					while(NRF24L01_RxPacket(g_RF24L01RxBuffer));//????????
//					wLength2 = 0;
//					//printf( "\r\nSerialnumberStr:\r\n");
//					wLength = ((uint16_t)g_RF24L01RxBuffer[1]<<8)+g_RF24L01RxBuffer[2];
//					for(i=3;i<32;i++)
//					{
//						//printf("0x%02X " ,(uint8_t)g_RF24L01RxBuffer[i]);
//						USBD_SERIALNUMBER_STRING_FS[wLength2++] = g_RF24L01RxBuffer[i];
//						if(--wLength==0)
//						{
//							i=32;
//						}
//					}
//					while(wLength!=0)
//					{
//						while(NRF24L01_RxPacket(g_RF24L01RxBuffer));
//						for(i=0;i<32;i++)
//						{
//							//printf("0x%02X " ,(uint8_t)g_RF24L01RxBuffer[i]);
//							USBD_SERIALNUMBER_STRING_FS[wLength2++] = g_RF24L01RxBuffer[i];
//							if(--wLength==0)
//							{
//								i=32;
//							}
//						}
//					}
//					while(NRF24L01_RxPacket(g_RF24L01RxBuffer));//????????
//					wLength2 = 0;
//					//printf( "\r\nHID_Report_Desc:\r\n");
//					wLength = ((uint16_t)g_RF24L01RxBuffer[1]<<8)+g_RF24L01RxBuffer[2];
//					TOUCH_SIZ_REPORT_DESC = wLength;
//					for(i=3;i<32;i++)
//					{
//						//printf("0x%02X " ,(uint8_t)g_RF24L01RxBuffer[i]);
//						HID_TOUCH_ReportDesc[wLength2++] = g_RF24L01RxBuffer[i];
//						if(--wLength==0)
//						{
//							i=32;
//						}
//					}
//					while(wLength!=0)
//					{
//						while(NRF24L01_RxPacket(g_RF24L01RxBuffer));
//						for(i=0;i<32;i++)
//						{
//							//printf("0x%02X " ,(uint8_t)g_RF24L01RxBuffer[i]);
//							HID_TOUCH_ReportDesc[wLength2++] = g_RF24L01RxBuffer[i];
//							if(--wLength==0)
//							{
//								i=32;
//							}
//						}
//					}
//					UsbHaveUpdate = 1;
//					
//					if(secdenum_f)
//					{
//						for(i=0;i<41;i++)
//						{
//							USBD_HID_CfgDesc[i] = USBD_HID_CfgDesc_mouse[i];
//						}
//					}
//					
//					if(usb_on==2)
//					{
//						//配對中
//					}
//					usb_on = 1;
//					USBD_LL_DevDisconnected(&hUsbDeviceFS);
//								MX_USB_DEVICE_Init();
//								USBD_Start(&hUsbDeviceFS);
//								UsbHaveUpdate = 0;
//					
//					
//					///USBD_LL_DevDisconnected(&hUsbDeviceFS);
//					///HAL_Delay(3000);
//					///MX_USB_DEVICE_Init();
//					///USBD_Start(&hUsbDeviceFS);
//				}
//			}
//}

void ParseHIDReport(void)
{
		TouchReportParsOk				= 0;
		TouchReportId						= 0xFF;
		TouchMaxNum							= 0xFF;
		TouchIndexBitOffset 		= 0xFFFF;
		TouchIndexBitSize 			= 0xFF;
		TouchTipSwitchBitOffset = 0xFFFF;
		TouchTipSwitchBitSize 	= 0xFF;
		TouchXBitOffset					= 0xFFFF;
		TouchXBitSize						= 0xFF;
		TouchYBitOffset					= 0xFFFF;
		TouchYBitOffset1				= 0xFFFF;
		TouchYBitOffset2				= 0xFFFF;
		TouchYBitSize						= 0xFF;
		TouchYBitSize1					= 0xFF;
		TouchTotalNum						= 0xFF;
		TouchTotalNumBiOffset 	= 0xFFFF;
		TouchTotalNumBitSize		= 0xFF;
	  TouchDatalen						= 0xFF;
			
		if(HID_PARSE_Sucessful==ProcessHIDReport(HID_TOUCH_ReportDesc,TOUCH_SIZ_REPORT_DESC,&ParserData))
		{
			unsigned int i,j;
			HID_ReportItem_t *pReportItemData;
			
			pReportItemData = ParserData.ReportItems;
			j = ParserData.TotalReportItems;		  
			for(i=0;i<j;i++,pReportItemData++)
			{
				if(pReportItemData->Attributes.Usage.Page==0x0001)
				{
					if((TouchReportId!=0xFF)&&(TouchReportId==pReportItemData->Attributes.ReportID))
					{
						if(pReportItemData->Attributes.Usage.Usage==0x0030)
						{
								if(TouchXBitSize==0xFF)
								{
									TouchXBitOffset = pReportItemData->BitOffset;
									TouchXBitSize		= pReportItemData->Attributes.BitSize;
									TouchXLogicMax	= pReportItemData->Attributes.Logical.Maximum;
								}
						}
						else if(pReportItemData->Attributes.Usage.Usage==0x0031)
						{
								if(TouchYBitSize==0xFF)
								{
									TouchYBitOffset = pReportItemData->BitOffset;
									TouchYBitSize		= pReportItemData->Attributes.BitSize;
									TouchYLogicMax	= pReportItemData->Attributes.Logical.Maximum;
								}
								else if(TouchYBitSize1==0xFF)
								{
									TouchYBitOffset1 = pReportItemData->BitOffset;
									TouchYBitSize1		= pReportItemData->Attributes.BitSize;
								}
								TouchYBitOffset2		= pReportItemData->BitOffset;
						}
					}
				}
				else if(pReportItemData->Attributes.Usage.Page==0x000D)
				{
					if(pReportItemData->Attributes.Usage.Usage==0x0051)
					{
							if(TouchIndexBitSize==0xFF)
							{
								if(TouchReportId==0xFF)
									TouchReportId = pReportItemData->Attributes.ReportID;
								TouchIndexBitOffset = pReportItemData->BitOffset;
								TouchIndexBitSize		= pReportItemData->Attributes.BitSize;
							}
					}
					else if(pReportItemData->Attributes.Usage.Usage==0x0042)
					{
							if(TouchTipSwitchBitSize==0xFF)
							{
								if(TouchReportId==0xFF)
									TouchReportId = pReportItemData->Attributes.ReportID;
								TouchTipSwitchBitOffset = pReportItemData->BitOffset;
								TouchTipSwitchBitSize		= pReportItemData->Attributes.BitSize;
							}
					}
					else if(pReportItemData->Attributes.Usage.Usage==0x0054)
					{
							if(TouchTotalNumBitSize==0xFF)
							{
								TouchTotalNumBiOffset = pReportItemData->BitOffset;
								TouchTotalNumBitSize  = pReportItemData->Attributes.BitSize;
							}
					}
//					else if(pReportItemData->Attributes.Usage.Usage==0x0055)
//					{
//							if(TouchMaxNum==0xFF)
//							{
//									TouchMaxNum = pReportItemData->Attributes.Logical.Maximum;
//							}
//					}
				}
			}
		}
		if((TouchYBitOffset!=0xFFFF)&&(TouchYBitOffset1!=0xFFFF)&&(TouchYBitOffset2!=0xFFFF))
		{
				TouchMaxNum = ((TouchYBitOffset2-TouchYBitOffset)/(TouchYBitOffset1-TouchYBitOffset))+1;
		}
		if((TouchYBitOffset1!=0xFFFF)&&(TouchYBitOffset!=0xFFFF))
		{
			TouchDataInc = TouchYBitOffset1-TouchYBitOffset;
			TouchDataStep = TouchDataInc/8;
		}
		
		if((TouchReportId!=0xFF)&&(TouchTipSwitchBitSize!=0xFF)&&(TouchIndexBitSize!=0xFF)&&(TouchXBitSize!=0xFF)&&(TouchYBitSize!=0xFF))
		{
				uint8_t i,maskbit;
			
				pTipSwitch = HIDSendReportData+1+(TouchTipSwitchBitOffset/8);
			  FirtTouchLen = 1+(TouchTipSwitchBitOffset/8);
				TipSwtichMask = 1<<(TouchTipSwitchBitOffset%8);
			  TipSwtichMaskRes = ~TipSwtichMask;
				
				pTouchId = HIDSendReportData+1+(TouchIndexBitOffset/8);
				maskbit = 1;
				for(i=0;i<(TouchIndexBitSize-1);i++)
				{
						maskbit <<= 1;
						maskbit |= 1;
				}
				TouchIdMask = maskbit << (TouchIndexBitOffset%8);
				
				pTouchXLow = HIDSendReportData+1+(TouchXBitOffset/8);
				pTouchXHi  = pTouchXLow+1;
				
				pTouchYLow = HIDSendReportData+1+(TouchYBitOffset/8);
				pTouchYHi  = pTouchYLow+1;
				
				HID_MOUSE_ReportDesc[43] = (uint8_t)TouchXLogicMax;
				HID_MOUSE_ReportDesc[44] = (uint8_t)(TouchXLogicMax>>8);
				
				HID_MOUSE_ReportDesc[48] = (uint8_t)TouchYLogicMax;
				HID_MOUSE_ReportDesc[49] = (uint8_t)(TouchYLogicMax>>8);
				
				TouchReportParsOk = 1;
		}
}

uint32_t GetDistant(uint16_t PtX1,uint16_t PtY1, uint16_t PtX2,uint16_t PtY2)
{
  	int32_t tmpdif1,tmpdif2;
	
  	tmpdif1 = PtX1 - PtX2;				  	//i,j
		tmpdif2 = PtY1 - PtY2;
		return (uint32_t)(tmpdif1*tmpdif1 + tmpdif2*tmpdif2);
}

uint8_t Right_Key_Function(uint16_t x, uint16_t y)
{
	uint8_t result = 0;
    
	if(right_key_press==0)
	{	
		start_position_x = x; 			// Contact_Info[WinXP_Valid_Index].X_Raw;
		start_position_y = y; 			// Contact_Info[WinXP_Valid_Index].Y_Raw;
		right_key_press=1;
		right_key_wait_up=0;
		right_press_time=RIGHT_KEY_DELAY;
	}
	else
	{
	  if(right_press_time==0)
		{	
		  	right_key_wait_up=1;
			  result = 1;
		}
		else if(GetDistant(start_position_x,start_position_y,x,y)>(2*RIGHT_BUTTON_RANGE*RIGHT_BUTTON_RANGE))
		{
		  	start_position_x = x; 			// Contact_Info[WinXP_Valid_Index].X_Raw;
			  start_position_y = y; 
			  right_press_time=RIGHT_KEY_DELAY;
		}
	}

	return result;
}

uint8_t new_data=0;
uint8_t last_data=0;
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(huart);
  /* NOTE: This function Should not be modified, when the callback is needed,
           the HAL_UART_RxCpltCallback could be implemented in the user file
   */
	if(huart->Instance == USART1)
	{
		Rx1buf[Rx1count] = aRx1buf[0];
		
		if((Rx1buf[Rx1count-1]==0x0D)&&(Rx1buf[Rx1count]==0x0A))
		{
			//HAL_UART_Transmit_IT(&huart1,(uint8_t *)Rx1buf,strlen((char *)Rx1buf));
			HAL_UART_Transmit_IT(&huart2,(uint8_t *)Rx1buf,strlen((char *)Rx1buf));
			Rx1count = 0;
		}
		else
		{
			Rx1count++;
		}
	}
	
	if(huart->Instance == USART2)
	{
		//if(Recever2OK==0)
		//{
///			if(Rx2timeout>10)
		///	{
				///Rx2count = 0;
		///	}
			//Rx2buf[Rx2count] = aRx2buf[0];
			new_data = aRx2buf[0];
			Rx2buf[Rx2bufHeadTmp++] = new_data;
			Rx2bufHeadTmp &= RXBUFSIZEMASK;
			//if((last_data==0x0D)&&(new_data==0x0A))
			//{
			Rx2bufHead = Rx2bufHeadTmp;
					//Recever2OK = 1;
			//}
			//else
			//{
			//	Rx2count++;
			//	Rx2timeout = 0;
			//}
			//last_data = new_data;
		//}
	}
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(huart);
  /* NOTE: This function Should not be modified, when the callback is needed,
           the HAL_UART_TxCpltCallback could be implemented in the user file
   */ 
}
/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.Prediv1Source = RCC_PREDIV1_SOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  RCC_OscInitStruct.PLL2.PLL2State = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV3;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

    /**Configure the Systick interrupt time 
    */
  __HAL_RCC_PLLI2S_ENABLE();

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
