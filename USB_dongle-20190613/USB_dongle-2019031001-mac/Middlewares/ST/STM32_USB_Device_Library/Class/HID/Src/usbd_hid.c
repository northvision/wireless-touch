/**
  ******************************************************************************
  * @file    usbd_hid.c
  * @author  MCD Application Team
  * @version V2.4.2
  * @date    11-December-2015
  * @brief   This file provides the HID core functions.
  *
  * @verbatim
  *      
  *          ===================================================================      
  *                                HID Class  Description
  *          =================================================================== 
  *           This module manages the HID class V1.11 following the "Device Class Definition
  *           for Human Interface Devices (HID) Version 1.11 Jun 27, 2001".
  *           This driver implements the following aspects of the specification:
  *             - The Boot Interface Subclass
  *             - The Mouse protocol
  *             - Usage Page : Generic Desktop
  *             - Usage : Joystick
  *             - Collection : Application 
  *      
  * @note     In HS mode and when the DMA is used, all variables and data structures
  *           dealing with the DMA during the transaction process should be 32-bit aligned.
  *           
  *      
  *  @endverbatim
  *
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "usbd_hid.h"
#include "usbd_desc.h"
#include "usbd_ctlreq.h"


/** @addtogroup STM32_USB_DEVICE_LIBRARY
  * @{
  */


/** @defgroup USBD_HID 
  * @brief usbd core module
  * @{
  */ 

/** @defgroup USBD_HID_Private_TypesDefinitions
  * @{
  */ 
/**
  * @}
  */ 


/** @defgroup USBD_HID_Private_Defines
  * @{
  */ 

/**
  * @}
  */ 


/** @defgroup USBD_HID_Private_Macros
  * @{
  */ 
/**
  * @}
  */ 




/** @defgroup USBD_HID_Private_FunctionPrototypes
  * @{
  */


static uint8_t  USBD_HID_Init (USBD_HandleTypeDef *pdev, 
                               uint8_t cfgidx);

static uint8_t  USBD_HID_DeInit (USBD_HandleTypeDef *pdev, 
                                 uint8_t cfgidx);

static uint8_t  USBD_HID_Setup (USBD_HandleTypeDef *pdev, 
                                USBD_SetupReqTypedef *req);

static uint8_t  *USBD_HID_GetCfgDesc (uint16_t *length);

static uint8_t  *USBD_HID_GetDeviceQualifierDesc (uint16_t *length);

static uint8_t  USBD_HID_DataIn (USBD_HandleTypeDef *pdev, uint8_t epnum);

uint16_t TOUCH_SIZ_REPORT_DESC = 0x02AD;//0x02DD;
/**
  * @}
  */ 

/** @defgroup USBD_HID_Private_Variables
  * @{
  */ 

USBD_ClassTypeDef  USBD_HID = 
{
  USBD_HID_Init,
  USBD_HID_DeInit,
  USBD_HID_Setup,
  NULL, /*EP0_TxSent*/  
  NULL, /*EP0_RxReady*/
  USBD_HID_DataIn, /*DataIn*/
  NULL, /*DataOut*/
  NULL, /*SOF */
  NULL,
  NULL,      
  USBD_HID_GetCfgDesc,
  USBD_HID_GetCfgDesc, 
  USBD_HID_GetCfgDesc,
  USBD_HID_GetDeviceQualifierDesc,
};

uint8_t USBD_HID_CfgDesc_mouse[41] =
{
  0x09, /* bLength: Configuration Descriptor size */
  USB_DESC_TYPE_CONFIGURATION, /* bDescriptorType: Configuration */
  41,
  /* wTotalLength: Bytes returned */
  0x00,
  0x01,         /*bNumInterfaces: 1 interface*/
  0x01,         /*bConfigurationValue: Configuration value*/
  0x00,         /*iConfiguration: Index of string descriptor describing
  the configuration*/
  0xA0,         /*bmAttributes: bus powered and Support Remote Wake-up */
  0xFA,         /*MaxPower 100 mA: this current is used for detecting Vbus*/
  
  /************** Descriptor of Joystick Mouse interface ****************/
  /* 09 */
  0x09,         /*bLength: Interface Descriptor size*/
  USB_DESC_TYPE_INTERFACE,/*bDescriptorType: Interface descriptor type*/
  0x00,         /*bInterfaceNumber: Number of Interface*/
  0x00,         /*bAlternateSetting: Alternate setting*/
  0x02,         /*bNumEndpoints*/
  0x03,         /*bInterfaceClass: HID*/
  0x01,         /*bInterfaceSubClass : 1=BOOT, 0=no boot*/
  0x02,         /*nInterfaceProtocol : 0=none, 1=keyboard, 2=mouse*/
  0,            /*iInterface: Index of string descriptor*/
  /******************** Descriptor of Joystick Mouse HID ********************/
  /* 18 */
  0x09,         /*bLength: HID Descriptor size*/
  HID_DESCRIPTOR_TYPE, /*bDescriptorType: HID*/
  0x11,         /*bcdHID: HID Class Spec release number*/
  0x01,
  0x00,         /*bCountryCode: Hardware target country*/
  0x01,         /*bNumDescriptors: Number of HID class descriptors to follow*/
  0x22,         /*bDescriptorType*/
  94,/*wItemLength: Total length of Report descriptor*/
  0x00,
  /******************** Descriptor of Mouse endpoint ********************/
  /* 27 */
  0x07,          /*bLength: Endpoint Descriptor size*/
  USB_DESC_TYPE_ENDPOINT, /*bDescriptorType:*/
  
  HID_EPIN_ADDR,     /*bEndpointAddress: Endpoint Address (IN)*/
  0x03,          /*bmAttributes: Interrupt endpoint*/
  HID_EPIN_SIZE, /*wMaxPacketSize: 4 Byte max */
  0x00,
  HID_FS_BINTERVAL,          /*bInterval: Polling Interval (10 ms)*/
  /* 34 */
	
	 0x07,          /*bLength: Endpoint Descriptor size*/
  USB_DESC_TYPE_ENDPOINT, /*bDescriptorType:*/
  
  HID_EPOUT_ADDR,     /*bEndpointAddress: Endpoint Address (IN)*/
  0x03,          /*bmAttributes: Interrupt endpoint*/
  HID_EPOUT_SIZE, /*wMaxPacketSize: 4 Byte max */
  0x00,
  HID_FS_BINTERVAL,          /*bInterval: Polling Interval (10 ms)*/
} ;
/* USB HID device Configuration Descriptor */
//__ALIGN_BEGIN static uint8_t USBD_HID_CfgDesc[USB_HID_CONFIG_DESC_SIZ]  __ALIGN_END =
uint8_t USBD_HID_CfgDesc[256] = 
{
  0x09, /* bLength: Configuration Descriptor size */
  USB_DESC_TYPE_CONFIGURATION, /* bDescriptorType: Configuration */
  0x41,
  /* wTotalLength: Bytes returned */
  0x00,
  0x01,         /*bNumInterfaces: 1 interface*/
  0x01,         /*bConfigurationValue: Configuration value*/
  0x00,         /*iConfiguration: Index of string descriptor describing
  the configuration*/
  0xA0,         /*bmAttributes: bus powered and Support Remote Wake-up */
  0xFA,         /*MaxPower 100 mA: this current is used for detecting Vbus*/
  
  /************** Descriptor of Joystick Mouse interface ****************/
  /* 09 */
  0x09,         /*bLength: Interface Descriptor size*/
  USB_DESC_TYPE_INTERFACE,/*bDescriptorType: Interface descriptor type*/
  0x00,         /*bInterfaceNumber: Number of Interface*/
  0x00,         /*bAlternateSetting: Alternate setting*/
  0x02,         /*bNumEndpoints*/
  0x03,         /*bInterfaceClass: HID*/
  0x00,         /*bInterfaceSubClass : 1=BOOT, 0=no boot*/
  0x00,         /*nInterfaceProtocol : 0=none, 1=keyboard, 2=mouse*/
  0,            /*iInterface: Index of string descriptor*/
  /******************** Descriptor of Joystick Mouse HID ********************/
  /* 18 */
  0x09,         /*bLength: HID Descriptor size*/
  HID_DESCRIPTOR_TYPE, /*bDescriptorType: HID*/
  0x11,         /*bcdHID: HID Class Spec release number*/
  0x01,
  0x00,         /*bCountryCode: Hardware target country*/
  0x01,         /*bNumDescriptors: Number of HID class descriptors to follow*/
  0x22,         /*bDescriptorType*/
  0xAD,//TOUCH_SIZ_REPORT_DESC_LO,/*wItemLength: Total length of Report descriptor*/
  0x02,//TOUCH_SIZ_REPORT_DESC_HI,
  /******************** Descriptor of Mouse endpoint ********************/
  /* 27 */
  0x07,          /*bLength: Endpoint Descriptor size*/
  USB_DESC_TYPE_ENDPOINT, /*bDescriptorType:*/
  
  HID_EPIN_ADDR,     /*bEndpointAddress: Endpoint Address (IN)*/
  0x03,          /*bmAttributes: Interrupt endpoint*/
  HID_EPIN_SIZE, /*wMaxPacketSize: 4 Byte max */
  0x00,
  HID_FS_BINTERVAL,          /*bInterval: Polling Interval (10 ms)*/
  /* 34 */
	
	 0x07,          /*bLength: Endpoint Descriptor size*/
  USB_DESC_TYPE_ENDPOINT, /*bDescriptorType:*/
  
  HID_EPOUT_ADDR,     /*bEndpointAddress: Endpoint Address (IN)*/
  0x03,          /*bmAttributes: Interrupt endpoint*/
  HID_EPOUT_SIZE, /*wMaxPacketSize: 4 Byte max */
  0x00,
  HID_FS_BINTERVAL,          /*bInterval: Polling Interval (10 ms)*/
} ;

/* USB HID device Configuration Descriptor */
//__ALIGN_BEGIN static uint8_t USBD_HID_Desc[USB_HID_DESC_SIZ]  __ALIGN_END  =
uint8_t USBD_HID_Desc[USB_HID_DESC_SIZ] =
{
  /* 18 */
  0x09,         /*bLength: HID Descriptor size*/
  HID_DESCRIPTOR_TYPE, /*bDescriptorType: HID*/
  0x11,         /*bcdHID: HID Class Spec release number*/
  0x01,
  0x00,         /*bCountryCode: Hardware target country*/
  0x01,         /*bNumDescriptors: Number of HID class descriptors to follow*/
  0x22,         /*bDescriptorType*/
  0xAD,//TOUCH_SIZ_REPORT_DESC_LO,/*wItemLength: Total length of Report descriptor*/
  0x02//TOUCH_SIZ_REPORT_DESC_HI,
};

/* USB Standard Device Descriptor */
__ALIGN_BEGIN static uint8_t USBD_HID_DeviceQualifierDesc[USB_LEN_DEV_QUALIFIER_DESC]  __ALIGN_END =
{
  USB_LEN_DEV_QUALIFIER_DESC,
  USB_DESC_TYPE_DEVICE_QUALIFIER,
  0x00,
  0x02,
  0x00,
  0x00,
  0x00,
  0x40,
  0x01,
  0x00,
};

__ALIGN_BEGIN uint8_t HID_MOUSE_ReportDesc[94] __ALIGN_END =
{
0x05, 0x01,  //Usage Page (Generic Desktop) //total 94bytes
0x09, 0x02,  //Usage (Mouse)
0xA1, 0x01,  //Collection (Application) 
0x09, 0x01,  //Usage (Pointer)
0xA1, 0x00, //Collection (Physical) 
0x85, 0x01,  //Report ID (1)
0x05, 0x09,  //Usage Page (Button) 
0x19, 0x01,  //Usage Minimum (Button 1)
0x29, 0x03,  //Usage Maximum (Button 3)
0x15, 0x00,  //Logical Minimum (0)
0x25, 0x01,  //Logical Maximum (1)
0x95, 0x03,  //Report Count (3)
0x75, 0x01, //Report Size (1)
0x81, 0x02,  //Input (Data,Var,Abs,NWrp,Lin,Pref,NNul,Bit)
0x95, 0x01,  //Report Count (1)
0x75, 0x05,  //Report Size (5)
0x81, 0x03,  //Input (Cnst,Var,Abs,NWrp,Lin,Pref,NNul,Bit)
0x05, 0x01,  //Usage Page (Generic Desktop)
0x09, 0x30,  //Usage (X)
0x09, 0x31,  //Usage (Y)
0x15, 0x00,  //Logical Minimum (0)
0x26, 0xFF, 0x7F,  //Logical Maximum (32767)
0x35, 0x00,  //Physical Minimum (0)
0x46, 0xFF, 0x7F,  //Physical Maximum (32767)
0x75, 0x10,  //Report Size (16)
0x95, 0x02,  //Report Count (2)
0x81, 0x02,  //Input (Data,Var,Abs,NWrp,Lin,Pref,NNul,Bit)
0x05, 0x0D,  //Usage Page (Digitizer)
0x09, 0x33,  //Usage (Touch)
0x15, 0x00,  //Logical Minimum (0)
0x26, 0xFF, 0x00,  //Logical Maximum (255)
0x35, 0x00,  //Physical Minimum (0)
0x46, 0xFF, 0x00,  //Physical Maximum (255)
0x75, 0x08,  //Report Size (8)
0x95, 0x01,  //Report Count (1)
0x81, 0x02,  //Input (Data,Var,Abs,NWrp,Lin,Pref,NNul,Bit)
0x05, 0x01,  //Usage Page (Generic Desktop)
0x09, 0x38,  //Usage (Wheel)
0x15, 0x81,  //Logical Minimum (-127)
0x25, 0x7F,  //Logical Maximum (127)
0x35, 0x81,  //Physical Minimum (-127)
0x45, 0x7F,  //Physical Maximum (127)
0x95, 0x01,  //Report Count (1)
0x81, 0x06,  //Input (Data,Var,Rel,NWrp,Lin,Pref,NNul,Bit)
0xC0,  //End Collection
0xC0,  //End Collection
};
#define TOUCH_POINT_10
//__ALIGN_BEGIN static uint8_t HID_MOUSE_ReportDesc[TOUCH_SIZ_REPORT_DESC]  __ALIGN_END 
//uint8_t HID_MOUSE_ReportDesc[1000] = 
uint8_t HID_TOUCH_ReportDesc[1000] = 
{
  0x05, 0x0d,                 //63             
0x09, 0x04,                              
0xa1, 0x01,                              
0x85, 0x02,                                                                    
0x09, 0x22,                    
0xa1, 0x02,		                              
0x09, 0x42,		                              
0x09, 0x32,                              
0x09, 0x47,
0x75, 0x01,                              
0x95, 0x03,                                                            
0x15, 0x00,                              
0x25, 0x01,                              		                              
0x81, 0x02,                              		
0x95, 0x05,                              
0x81, 0x03,                              
0x09, 0x51,
0x75, 0x08,                                                                 		                              
0x95, 0x01,                              
0x81, 0x02,	                              		                              
0x05, 0x01,
0x09, 0x30,
0x09, 0x31,
0x75, 0x10,
0x95, 0x02,                                                                           
0x16, 0x00, 0x00,  
0x26, 0xff, 0x7f,   
0x46, 0x00, 0x00,
0xa4,			                                                                                             
0x81, 0x02, 		                                                    
0xc0,                                                                    
#ifdef TOUCH_POINT_10 
0xa1, 0x02,		//24   ????
0x05, 0x0d,		                              
0x09, 0x42,		                              
0x09, 0x32,                              
0x09, 0x47,
0x75, 0x01,                              
0x95, 0x03,                                                            
0x15, 0x00,                              
0x25, 0x01,                              		                              
0x81, 0x02,                              		
0x95, 0x05,                              
0x81, 0x03,                              

0x09, 0x51,		//8
0x75, 0x08,                                                                 		                              
0x95, 0x01,                              
0x81, 0x02,
	                              		                              
0xb4,			//9
0xa4,
0x09, 0x30,
0x09, 0x31,                                                                                             
0x81, 0x02, 		                                                    
0xc0,              
       
0xa1, 0x02,		//24   ????
0x05, 0x0d,		                              
0x09, 0x42,		                              
0x09, 0x32,                              
0x09, 0x47,
0x75, 0x01,                              
0x95, 0x03,                                                            
0x15, 0x00,                              
0x25, 0x01,                              		                              
0x81, 0x02,                              		
0x95, 0x05,                              
0x81, 0x03,                              

0x09, 0x51,		//8
0x75, 0x08,                                                                 		                              
0x95, 0x01,                              
0x81, 0x02,
		                              		                              
0xb4,			//9
0xa4,
0x09, 0x30,
0x09, 0x31,                                                                                             
0x81, 0x02, 		                                                    
0xc0,

0xa1, 0x02,		//24   ????
0x05, 0x0d,		                              
0x09, 0x42,		                              
0x09, 0x32,                              
0x09, 0x47,
0x75, 0x01,                              
0x95, 0x03,                                                            
0x15, 0x00,                              
0x25, 0x01,                              		                              
0x81, 0x02,                              		
0x95, 0x05,                              
0x81, 0x03,                              

0x09, 0x51,		//8
0x75, 0x08,                                                                 		                              
0x95, 0x01,                              
0x81, 0x02,
	                              		                              
0xb4,			//9
0xa4,
0x09, 0x30,
0x09, 0x31,                                                                                             
0x81, 0x02, 		                                                    
0xc0,              
       
0xa1, 0x02,		//24   ????
0x05, 0x0d,		                              
0x09, 0x42,		                              
0x09, 0x32,                              
0x09, 0x47,
0x75, 0x01,                              
0x95, 0x03,                                                            
0x15, 0x00,                              
0x25, 0x01,                              		                              
0x81, 0x02,                              		
0x95, 0x05,                              
0x81, 0x03,                              

0x09, 0x51,		//8
0x75, 0x08,                                                                 		                              
0x95, 0x01,                              
0x81, 0x02,
		                              		                              
0xb4,			//9
0xa4,
0x09, 0x30,
0x09, 0x31,                                                                                             
0x81, 0x02, 		                                                    
0xc0,

0xa1, 0x02,		//24   ????
0x05, 0x0d,		                              
0x09, 0x42,		                              
0x09, 0x32,                              
0x09, 0x47,
0x75, 0x01,                              
0x95, 0x03,                                                            
0x15, 0x00,                              
0x25, 0x01,                              		                              
0x81, 0x02,                              		
0x95, 0x05,                              
0x81, 0x03,                              

0x09, 0x51,		//8
0x75, 0x08,                                                                 		                              
0x95, 0x01,                              
0x81, 0x02,
	                              		                              
0xb4,			//9
0xa4,
0x09, 0x30,
0x09, 0x31,                                                                                             
0x81, 0x02, 		                                                    
0xc0,              
       
0xa1, 0x02,		//24   ????
0x05, 0x0d,		                              
0x09, 0x42,		                              
0x09, 0x32,                              
0x09, 0x47,
0x75, 0x01,                              
0x95, 0x03,                                                            
0x15, 0x00,                              
0x25, 0x01,                              		                              
0x81, 0x02,                              		
0x95, 0x05,                              
0x81, 0x03,                              

0x09, 0x51,		//8
0x75, 0x08,                                                                 		                              
0x95, 0x01,                              
0x81, 0x02,
		                              		                              
0xb4,			//9
0xa4,
0x09, 0x30,
0x09, 0x31,                                                                                             
0x81, 0x02, 		                                                    
0xc0,

0xa1, 0x02,		//24   ????
0x05, 0x0d,		                              
0x09, 0x42,		                              
0x09, 0x32,                              
0x09, 0x47,
0x75, 0x01,                              
0x95, 0x03,                                                            
0x15, 0x00,                              
0x25, 0x01,                              		                              
0x81, 0x02,                              		
0x95, 0x05,                              
0x81, 0x03,                              

0x09, 0x51,		//8
0x75, 0x08,                                                                 		                              
0x95, 0x01,                              
0x81, 0x02,
	                              		                              
0xb4,			//9
0xa4,
0x09, 0x30,
0x09, 0x31,                                                                                             
0x81, 0x02, 		                                                    
0xc0,              
       
0xa1, 0x02,		//24   ????
0x05, 0x0d,		                              
0x09, 0x42,		                              
0x09, 0x32,                              
0x09, 0x47,
0x75, 0x01,                              
0x95, 0x03,                                                            
0x15, 0x00,                              
0x25, 0x01,                              		                              
0x81, 0x02,                              		
0x95, 0x05,                              
0x81, 0x03,                              

0x09, 0x51,		//8
0x75, 0x08,                                                                 		                              
0x95, 0x01,                              
0x81, 0x02,
		                              		                              
0xb4,			//9
0xa4,
0x09, 0x30,
0x09, 0x31,                                                                                             
0x81, 0x02, 		                                                    
0xc0,        
#elif defined(TOUCH_POINT_6)

0xa1, 0x02,		//24   ????
0x05, 0x0d,		                              
0x09, 0x42,		                              
0x09, 0x32,                              
0x09, 0x47,
0x75, 0x01,                              
0x95, 0x03,                                                            
0x15, 0x00,                              
0x25, 0x01,                              		                              
0x81, 0x02,                              		
0x95, 0x05,                              
0x81, 0x03,                              

0x09, 0x51,		//8
0x75, 0x08,                                                                 		                              
0x95, 0x01,                              
0x81, 0x02,
	                              		                              
0xb4,			//9
0xa4,
0x09, 0x30,
0x09, 0x31,                                                                                             
0x81, 0x02, 		                                                    
0xc0,              
       
0xa1, 0x02,		//24   ????
0x05, 0x0d,		                              
0x09, 0x42,		                              
0x09, 0x32,                              
0x09, 0x47,
0x75, 0x01,                              
0x95, 0x03,                                                            
0x15, 0x00,                              
0x25, 0x01,                              		                              
0x81, 0x02,                              		
0x95, 0x05,                              
0x81, 0x03,                              

0x09, 0x51,		//8
0x75, 0x08,                                                                 		                              
0x95, 0x01,                              
0x81, 0x02,
		                              		                              
0xb4,			//9
0xa4,
0x09, 0x30,
0x09, 0x31,                                                                                             
0x81, 0x02, 		                                                    
0xc0,

0xa1, 0x02,		//24   ????
0x05, 0x0d,		                              
0x09, 0x42,		                              
0x09, 0x32,                              
0x09, 0x47,
0x75, 0x01,                              
0x95, 0x03,                                                            
0x15, 0x00,                              
0x25, 0x01,                              		                              
0x81, 0x02,                              		
0x95, 0x05,                              
0x81, 0x03,                              

0x09, 0x51,		//8
0x75, 0x08,                                                                 		                              
0x95, 0x01,                              
0x81, 0x02,
	                              		                              
0xb4,			//9
0xa4,
0x09, 0x30,
0x09, 0x31,                                                                                             
0x81, 0x02, 		                                                    
0xc0,              
       
0xa1, 0x02,		//24   ????
0x05, 0x0d,		                              
0x09, 0x42,		                              
0x09, 0x32,                              
0x09, 0x47,
0x75, 0x01,                              
0x95, 0x03,                                                            
0x15, 0x00,                              
0x25, 0x01,                              		                              
0x81, 0x02,                              		
0x95, 0x05,                              
0x81, 0x03,                              

0x09, 0x51,		//8
0x75, 0x08,                                                                 		                              
0x95, 0x01,                              
0x81, 0x02,
		                              		                              
0xb4,			//9
0xa4,
0x09, 0x30,
0x09, 0x31,                                                                                             
0x81, 0x02, 		                                                    
0xc0,
//227
#elif defined(TOUCH_POINT_4)

0xa1, 0x02,		//24   ????
0x05, 0x0d,		                              
0x09, 0x42,		                              
0x09, 0x32,                              
0x09, 0x47,
0x75, 0x01,                              
0x95, 0x03,                                                            
0x15, 0x00,                              
0x25, 0x01,                              		                              
0x81, 0x02,                              		
0x95, 0x05,                              
0x81, 0x03,                              

0x09, 0x51,		//8
0x75, 0x08,                                                                 		                              
0x95, 0x01,                              
0x81, 0x02,
	                              		                              
0xb4,			//9
0xa4,
0x09, 0x30,
0x09, 0x31,                                                                                             
0x81, 0x02, 		                                                    
0xc0,              
       
0xa1, 0x02,		//24   ????
0x05, 0x0d,		                              
0x09, 0x42,		                              
0x09, 0x32,                              
0x09, 0x47,
0x75, 0x01,                              
0x95, 0x03,                                                            
0x15, 0x00,                              
0x25, 0x01,                              		                              
0x81, 0x02,                              		
0x95, 0x05,                              
0x81, 0x03,                              

0x09, 0x51,		//8
0x75, 0x08,                                                                 		                              
0x95, 0x01,                              
0x81, 0x02,
		                              		                              
0xb4,			//9
0xa4,
0x09, 0x30,
0x09, 0x31,                                                                                             
0x81, 0x02, 		                                                    
0xc0,
//145
#else
//63
#endif 
		
0xa1, 0x02,		//24   ????
0x05, 0x0d,		                              
0x09, 0x42,		                              
0x09, 0x32,                              
0x09, 0x47,
0x75, 0x01,                              
0x95, 0x03,                                                            
0x15, 0x00,                              
0x25, 0x01,                              		                              
0x81, 0x02,                              		
0x95, 0x05,                              
0x81, 0x03,                              

0x09, 0x51,		//8
0x75, 0x08,                                                                 		                              
0x95, 0x01,                              
0x81, 0x02,
		                              		                              
0xb4,			//8
//0xa4,
0x09, 0x30,
0x09, 0x31,                                                                                             
0x81, 0x02, 		                                                    
0xc0,
//40				                   
0x05, 0x0d,  //   USAGE_PAGE (Digitizers)                                   
0x09, 0x54,  //   USAGE (Actual (contact) count)
0x09, 0x01,  //   USAGE(Digitizers)                                                                    
0x95, 0x01,  //   REPORT_COUNT (8)                                  
0x75, 0x08,  //   REPORT_SIZE (8)                                  
0x15, 0x00,    //       LOGICAL_MINIMUM (0)                                 
0x25, 0x80,     //       LOGICAL_MAXIMUM (8)                                                                 
0x81, 0x02,     //   INPUT (Data,Var,Abs)                               
0x85, 0x03,     //   REPORT_ID (Feature)                                
0x09, 0x55,    //   USAGE(Maximum Count)                                
0xb1, 0x02,   //   FEATURE (Data,Var,Abs)
	                                                                      
0xc0, 
//63
                                         
0x09, 0x0e,   //26 // USAGE (Device Configuration)                                
0xa1, 0x01,   // COLLECTION (Application)                                 
0x85, 0x04,   //   REPORT_ID (Configuration)                                    
0x09, 0x23,     //   USAGE (Device Settings)                                 
0xa1, 0x02,      //   COLLECTION (logical)                                
0x09, 0x52,      //    USAGE (Device Mode)                      
0x09, 0x53,     //    USAGE (Device Identifier)                     
0x15, 0x00,     //    LOGICAL_MINIMUM (0)                     
0x25, 0x0a,     //    LOGICAL_MAXIMUM (10)                      
0x75, 0x08,     //    REPORT_SIZE (8)                       
0x95, 0x02,     //    REPORT_COUNT (2)                      
0xb1, 0x02,      //   FEATURE (Data,Var,Abs)                       
0xc0,  
0xc0,

0x05, 0x01,  //Usage Page (Generic Desktop) //total 94bytes
0x09, 0x02,  //Usage (Mouse)
0xA1, 0x01,  //Collection (Application) 
0x09, 0x01,  //Usage (Pointer)
0xA1, 0x00, //Collection (Physical) 
0x85, 0x01,  //Report ID (1)
0x05, 0x09,  //Usage Page (Button) 
0x19, 0x01,  //Usage Minimum (Button 1)
0x29, 0x03,  //Usage Maximum (Button 3)
0x15, 0x00,  //Logical Minimum (0)
0x25, 0x01,  //Logical Maximum (1)
0x95, 0x03,  //Report Count (3)
0x75, 0x01, //Report Size (1)
0x81, 0x02,  //Input (Data,Var,Abs,NWrp,Lin,Pref,NNul,Bit)
0x95, 0x01,  //Report Count (1)
0x75, 0x05,  //Report Size (5)
0x81, 0x03,  //Input (Cnst,Var,Abs,NWrp,Lin,Pref,NNul,Bit)
0x05, 0x01,  //Usage Page (Generic Desktop)
0x09, 0x30,  //Usage (X)
0x09, 0x31,  //Usage (Y)
0x15, 0x00,  //Logical Minimum (0)
0x26, 0xFF, 0x7F,  //Logical Maximum (32767)
0x35, 0x00,  //Physical Minimum (0)
0x46, 0xFF, 0x7F,  //Physical Maximum (32767)
0x75, 0x10,  //Report Size (16)
0x95, 0x02,  //Report Count (2)
0x81, 0x02,  //Input (Data,Var,Abs,NWrp,Lin,Pref,NNul,Bit)
0x05, 0x0D,  //Usage Page (Digitizer)
0x09, 0x33,  //Usage (Touch)
0x15, 0x00,  //Logical Minimum (0)
0x26, 0xFF, 0x00,  //Logical Maximum (255)
0x35, 0x00,  //Physical Minimum (0)
0x46, 0xFF, 0x00,  //Physical Maximum (255)
0x75, 0x08,  //Report Size (8)
0x95, 0x01,  //Report Count (1)
0x81, 0x02,  //Input (Data,Var,Abs,NWrp,Lin,Pref,NNul,Bit)
0x05, 0x01,  //Usage Page (Generic Desktop)
0x09, 0x38,  //Usage (Wheel)
0x15, 0x81,  //Logical Minimum (-127)
0x25, 0x7F,  //Logical Maximum (127)
0x35, 0x81,  //Physical Minimum (-127)
0x45, 0x7F,  //Physical Maximum (127)
0x95, 0x01,  //Report Count (1)
0x81, 0x06,  //Input (Data,Var,Rel,NWrp,Lin,Pref,NNul,Bit)
0xC0,  //End Collection
0xC0,  //End Collection
//183

//0x05, 0x01,   //60	//Usage Page (Generic Desktop)                                
//0x09, 0x02,  		//Usage (Mouse)                                 
//0xa1, 0x01, 		//Collection (Application)                                  
//0x85, 0x01,                                   
//0x09, 0x01,                                   
//0xa1, 0x00,                                   
//0x05 ,0x09,                                   
//0x19, 0x01,                                   
//0x29, 0x03,                                   
//0x15, 0x00,                                   
//0x25, 0x01,                                   
//0x95, 0x03,                                   
//0x75, 0x01,                                   
//0x81, 0x02,                                   
//0x95, 0x01,                                   
//0x75, 0x05,                                   
//0x81, 0x01,                                   
//0x05, 0x01,                                   
//0x09, 0x30,                                   
//0x09, 0x31,                       
//0x16, 0x00, 0x00,                               
//0x26, 0xff, 0x7f,                               
//0x36, 0x00, 0x00,                               
//0x46, 0xff, 0x7f,                                                             
//0x75, 0x10,                            
//0x95, 0x02,                            
//0x81, 0x62,                                                                      
//0xc0,  
//0xc0, 

	//67
//	0x05,0x01,			//Usage Page (Generic Desktop)   
//	0x09,0x06,			//Usage (Keyboard)   
//	0xA1,0x01,			//Collection (Application)   
//    0x85,0x05,			//Report ID (5)   
//    0x05,0x07,			//Usage Page (Keyboard/Keypad)   
//    0x19,0xE0,			//Usage Minimum (Keyboard Left Control)   
//    0x29,0xE7,			//Usage Maximum (Keyboard Right GUI)   
//    0x15,0x00,			//Logical Minimum (0)   
//    0x25,0x01,			//Logical Maximum (1)   
//    0x95,0x08,			//Report Count (8)   
//    0x75,0x01,			//Report Size (1)   
//    0x81,0x02,			//Input (Data,Var,Abs,NWrp,Lin,Pref,NNul,Bit)   
//    0x95,0x01,			//Report Count (1)   
//    0x75,0x08,			//Report Size (8)   
//    0x81,0x01,			//Input (Cnst,Var,Abs,NWrp,Lin,Pref,NNul,Bit)   
//    0x95,0x05,			//Report Count (6)   
//    0x75,0x08,			//Report Size (8)   
//    0x15,0x00,			//Logical Minimum (0)   
//    0x25,0x65,			//Logical Maximum (-1)   
//    0x05,0x07,			//Usage Page (Keyboard/Keypad)   
//    0x19,0x00,			//Usage Minimum (Undefined)   
//    0x29,0x65,			//Usage Maximum (Keyboard Application)   
//    0x81,0x00,			//Input (Data,Ary,Abs)  
////    0x25,0x01,			//Logical Maximum (1)   
////    0x95,0x05,			//Report Count (5)   
////    0x75,0x01,			//Report Size (1)   
////    0x05,0x08,			//Usage Page (LEDs)   
////    0x19,0x01,			//Usage Minimum (Num Lock)   
////    0x29,0x05,			//Usage Maximum (Kana)   
////    0x91,0x02,			//Output (Data,Var,Abs,NWrp,Lin,Pref,NNul,NVol,Bit)   
////    0x95,0x01,			//Report Count (1)   
////    0x75,0x03,			//Report Size (3)   
////    0x91,0x03,			//Output (Cnst,Var,Abs,NWrp,Lin,Pref,NNul,NVol,Bit)   
//	0xC0,				//End Collection 	

0x06,0x00,0xff,	//36
0x09,0x00,
0xA1,0x01,
0x85,0x06,
0x09,0x01,
0x15,0x00,
0x26,0xFF,0x00,
0x75,0x08,
0x95,0x3f,
0x81,0x02,
0x09,0x02,
0x15,0x00,
0x26,0xFF,0x00,
0x75,0x08,
0x95,0x3f,
0x91,0x02,
0xc0,
//219
0x06,0x00,0xff,	//36
0x09,0x00,
0xA1,0x01,
0x85,0x07,
0x09,0x01,
0x15,0x00,
0x26,0xFF,0x00,
0x75,0x08,
0x95,0x0f,
0x81,0x02,
0x09,0x02,
0x15,0x00,
0x26,0xFF,0x00,
0x75,0x08,
0x95,0x0f,
0x91,0x02,
0xc0,                           // END_COLLECTION
/*
BYTE1 --
       |--bit0:   Left Control????,???1 
       |--bit1:   Left Shift  ????,???1 
       |--bit2:   Left Alt    ????,???1 
       |--bit3:   Left GUI    ????,???1 
       |--bit4:   Right Control????,???1  
       |--bit5:   Right Shift ????,???1 
       |--bit6:   Right Alt   ????,???1 
       |--bit7:   Right GUI   ????,???1 
BYTE2 -- ???
BYTE3--BYTE8 -- ????????
*/
0x05,0x01,			//Usage Page (Generic Desktop)   
0x09,0x06,			//Usage (Keyboard)   
0xA1,0x01,			//Collection (Application)   
0x85,0x05,			//Report ID (5)   
0x05,0x07,			//Usage Page (Keyboard/Keypad)   
0x19,0xE0,			//Usage Minimum (Keyboard Left Control)   
0x29,0xE7,			//Usage Maximum (Keyboard Right GUI)   
0x15,0x00,			//Logical Minimum (0)   
0x25,0x01,			//Logical Maximum (1)   
0x95,0x08,			//Report Count (8)   
0x75,0x01,			//Report Size (1)   
0x81,0x02,			//Input (Data,Var,Abs,NWrp,Lin,Pref,NNul,Bit)   
0x95,0x02,			//Report Count (1)   
0x75,0x08,			//Report Size (8)   
0x15,0x00,			//Logical Minimum (0)   
0x25,0x7F,			//Logical Maximum (-1)   
0x19,0x00,			//Usage Minimum (Undefined)   
0x29,0x7F,			//Usage Maximum (Keyboard Application)   
0x81,0x00,			//Input (Data,Ary,Abs)  
0xC0,
////255
}; 

__ALIGN_BEGIN uint8_t Touch_GetReport_MaxContactDescriptor[TOUCH_SIZ_GETREPORT_MAXCONTACT_DESC] __ALIGN_END =
  {
    0x03,                       /*Report ID */
#ifdef TOUCH_POINT_10
	0x0a,                        /*Touch max contacts*/
#elif defined(TOUCH_POINT_6)
	0x06,                        /*Touch max contacts*/
#elif defined(TOUCH_POINT_4)
	0x04,                        /*Touch max contacts*/
#else
	0x02,                        /*Touch max contacts*/
#endif
	0x00,
  }; /* Touch_GetReportDescriptor */

#if 0
__ALIGN_BEGIN static uint8_t Touch_GetReport_SetModeXPDescriptor[TOUCH_SIZ_GETREPORT_SETMODE_DESC] __ALIGN_END =
  {
    0x04,                       /*Report ID */
    0x01,                        /*set mode report id*/
	  0x00,
  }; /* Touch_GetReportDescriptor */
#endif  
__ALIGN_BEGIN static uint8_t Touch_GetReport_SetMOdeWin7Descriptor[TOUCH_SIZ_GETREPORT_SETMODE_DESC] __ALIGN_END =
  {
    0x04,                       /*Report ID */
    0x02,                        /*set mode report id*/
	  0x00,
  }; /* Touch_GetReportDescriptor */

/**
  * @}
  */ 

/** @defgroup USBD_HID_Private_Functions
  * @{
  */ 

/**
  * @brief  USBD_HID_Init
  *         Initialize the HID interface
  * @param  pdev: device instance
  * @param  cfgidx: Configuration index
  * @retval status
  */
static uint8_t  USBD_HID_Init (USBD_HandleTypeDef *pdev, 
                               uint8_t cfgidx)
{
  uint8_t ret = 0;
  
  /* Open EP IN */
  USBD_LL_OpenEP(pdev,
                 HID_EPIN_ADDR,
                 USBD_EP_TYPE_INTR,
                 HID_EPIN_SIZE);  
  
  pdev->pClassData = USBD_malloc(sizeof (USBD_HID_HandleTypeDef));
  
  if(pdev->pClassData == NULL)
  {
    ret = 1; 
  }
  else
  {
    ((USBD_HID_HandleTypeDef *)pdev->pClassData)->state = HID_IDLE;
  }
  return ret;
}

/**
  * @brief  USBD_HID_Init
  *         DeInitialize the HID layer
  * @param  pdev: device instance
  * @param  cfgidx: Configuration index
  * @retval status
  */
static uint8_t  USBD_HID_DeInit (USBD_HandleTypeDef *pdev, 
                                 uint8_t cfgidx)
{
  /* Close HID EPs */
  USBD_LL_CloseEP(pdev,
                  HID_EPIN_ADDR);
  
  /* FRee allocated memory */
  if(pdev->pClassData != NULL)
  {
    USBD_free(pdev->pClassData);
    pdev->pClassData = NULL;
  } 
  
  return USBD_OK;
}

volatile uint32_t HIDApplicationModeChanging,setidle_reqed_f,MacOs_f,secdenum_f,secdenumbg_f;
volatile uint8_t SetReportID;
__ALIGN_BEGIN static uint8_t hid_mod_buf[12] __ALIGN_END;
extern uint8_t Touch_GetReport_MaxContactDescriptor[TOUCH_SIZ_GETREPORT_MAXCONTACT_DESC];
/**
  * @brief  USBD_HID_Setup
  *         Handle the HID specific requests
  * @param  pdev: instance
  * @param  req: usb requests
  * @retval status
  */
static uint8_t  USBD_HID_Setup (USBD_HandleTypeDef *pdev, 
                                USBD_SetupReqTypedef *req)
{
  uint16_t len = 0;
  uint8_t  *pbuf = NULL;
  USBD_HID_HandleTypeDef     *hhid = (USBD_HID_HandleTypeDef*) pdev->pClassData;
  
  switch (req->bmRequest & USB_REQ_TYPE_MASK)
  {
  case USB_REQ_TYPE_CLASS :  
    switch (req->bRequest)
    {
      
      
    case HID_REQ_SET_PROTOCOL:
      hhid->Protocol = (uint8_t)(req->wValue);
      break;
      
    case HID_REQ_GET_PROTOCOL:
      USBD_CtlSendData (pdev, 
                        (uint8_t *)&hhid->Protocol,
                        1);    
      break;
      
    case HID_REQ_SET_IDLE:
      hhid->IdleState = (uint8_t)(req->wValue >> 8);
		  setidle_reqed_f = 1;
      break;
      
    case HID_REQ_GET_IDLE:
      USBD_CtlSendData (pdev, 
                        (uint8_t *)&hhid->IdleState,
                        1);        
      break; 

		case HID_REQ_GET_REPORT:
			if(0x0300==(req->wValue & 0xff00))// ==(0x0300 + 0x003))
			{
						pbuf = Touch_GetReport_MaxContactDescriptor;
						Touch_GetReport_MaxContactDescriptor[0] = (uint8_t)req->wValue;
			}
			else if(0x0400==(req->wValue & 0xff00))//(req->wValue ==(0x0400 + 0x003))
			{
					pbuf = Touch_GetReport_SetMOdeWin7Descriptor;
					Touch_GetReport_SetMOdeWin7Descriptor[0] = (uint8_t)req->wValue;
			}
			else
			{
					 pbuf = Touch_GetReport_MaxContactDescriptor;
				   Touch_GetReport_MaxContactDescriptor[0] = (uint8_t)req->wValue;
			}	
			//len = 2;//MIN(2 , req->wLength);
			len = req->wLength;
			USBD_CtlSendData (pdev, 
													pbuf,
													len);
		break;
			
		case HID_REQ_SET_REPORT:
	  if(0x0300==(req->wValue & 0xff00))//if(req->wValue ==(0x0300 + 0x04))
	  {
			SetReportID = (uint8_t)req->wValue;
			HIDApplicationModeChanging = 1;
			USBD_CtlPrepareRx (pdev,
                             (uint8_t*)hid_mod_buf,
                             req->wLength);
	  }
	  break;
      
    default:
      USBD_CtlError (pdev, req);
      return USBD_FAIL; 
    }
    break;
    
  case USB_REQ_TYPE_STANDARD:
    switch (req->bRequest)
    {
    case USB_REQ_GET_DESCRIPTOR: 
      if( req->wValue >> 8 == HID_REPORT_DESC)
      {
        //len = MIN(TOUCH_SIZ_REPORT_DESC , req->wLength);
        //pbuf = HID_MOUSE_ReportDesc;
//				if(setidle_reqed_f==0)
//				{
//					len = MIN(94 , req->wLength);
//					//if(req->wLength==94)
//					//	secdenum_f = 0;
//					pbuf = HID_MOUSE_ReportDesc;
//					MacOs_f = 1;
//				}
//				else
//				{
//					len = MIN(TOUCH_SIZ_REPORT_DESC , req->wLength);
//					pbuf = HID_TOUCH_ReportDesc;
//				}
				
				if(req->wLength == 94)
				{
						len = MIN(94 , req->wLength);
						secdenum_f = 0;
						pbuf = HID_MOUSE_ReportDesc;
				}
				else
				{
						len = MIN(TOUCH_SIZ_REPORT_DESC , req->wLength);
						pbuf = HID_TOUCH_ReportDesc;
				}
//				if(secdenum_f)
//				{
//					len = MIN(94 , req->wLength);
//					if(req->wLength==94)
//						secdenum_f = 0;
//					pbuf = HID_MOUSE_ReportDesc;
//				}
//				else
//				{
//					len = MIN(TOUCH_SIZ_REPORT_DESC , req->wLength);
//					pbuf = HID_TOUCH_ReportDesc;
//				}
      }
      else if( req->wValue >> 8 == HID_DESCRIPTOR_TYPE)
      {
        pbuf = USBD_HID_Desc;   
        len = MIN(USB_HID_DESC_SIZ , req->wLength);
      }
      
      USBD_CtlSendData (pdev, 
                        pbuf,
                        len);
      
      break;
      
    case USB_REQ_GET_INTERFACE :
      USBD_CtlSendData (pdev,
                        (uint8_t *)&hhid->AltSetting,
                        1);
      break;
      
    case USB_REQ_SET_INTERFACE :
      hhid->AltSetting = (uint8_t)(req->wValue);
      break;
    }
  }
  return USBD_OK;
}

void chk_mode_setting(void)
{
	if(HIDApplicationModeChanging)
	{
		if(hid_mod_buf[0] == SetReportID)//0x04)	//configuration setting feature Report ID = 0x04
		{	
			//if(hid_mod_buf[1] == 0x01)
			//{
				if(setidle_reqed_f==0)
				{
					secdenum_f = 1;	
					secdenumbg_f = 1;
					MacOs_f = 1;
				}
			//}
		}
		HIDApplicationModeChanging = 0;
//		if(ScanLockCnt) ScanLockCnt--;
	}
}

/**
  * @brief  USBD_HID_SendReport 
  *         Send HID Report
  * @param  pdev: device instance
  * @param  buff: pointer to report
  * @retval status
  */
uint8_t USBD_HID_SendReport     (USBD_HandleTypeDef  *pdev, 
                                 uint8_t *report,
                                 uint16_t len)
{
  USBD_HID_HandleTypeDef     *hhid = (USBD_HID_HandleTypeDef*)pdev->pClassData;
  
  if (pdev->dev_state == USBD_STATE_CONFIGURED )
  {
    if(hhid->state == HID_IDLE)
    {
      hhid->state = HID_BUSY;
      USBD_LL_Transmit (pdev, 
                        HID_EPIN_ADDR,                                      
                        report,
                        len);
    }
  }
  return USBD_OK;
}

/**
  * @brief  USBD_HID_GetPollingInterval 
  *         return polling interval from endpoint descriptor
  * @param  pdev: device instance
  * @retval polling interval
  */
uint32_t USBD_HID_GetPollingInterval (USBD_HandleTypeDef *pdev)
{
  uint32_t polling_interval = 0;

  /* HIGH-speed endpoints */
  if(pdev->dev_speed == USBD_SPEED_HIGH)
  {
   /* Sets the data transfer polling interval for high speed transfers. 
    Values between 1..16 are allowed. Values correspond to interval 
    of 2 ^ (bInterval-1). This option (8 ms, corresponds to HID_HS_BINTERVAL */
    polling_interval = (((1 <<(HID_HS_BINTERVAL - 1)))/8);
  }
  else   /* LOW and FULL-speed endpoints */
  {
    /* Sets the data transfer polling interval for low and full 
    speed transfers */
    polling_interval =  HID_FS_BINTERVAL;
  }
  
  return ((uint32_t)(polling_interval));
}

/**
  * @brief  USBD_HID_GetCfgDesc 
  *         return configuration descriptor
  * @param  speed : current device speed
  * @param  length : pointer data length
  * @retval pointer to descriptor buffer
  */
static uint8_t  *USBD_HID_GetCfgDesc (uint16_t *length)
{
	if(secdenum_f)
  {
		USBD_HID_CfgDesc[25] = 94;//MOUSE_SIZ_REPORT_DESC_LO;
		USBD_HID_CfgDesc[26] = 0;//MOUSE_SIZ_REPORT_DESC_HI;
  }
  *length = sizeof (USBD_HID_CfgDesc);
  return USBD_HID_CfgDesc;
}


/**
  * @brief  USBD_HID_DataIn
  *         handle data IN Stage
  * @param  pdev: device instance
  * @param  epnum: endpoint index
  * @retval status
  */
static uint8_t  USBD_HID_DataIn (USBD_HandleTypeDef *pdev, 
                              uint8_t epnum)
{
  
  /* Ensure that the FIFO is empty before a new transfer, this condition could 
  be caused by  a new transfer before the end of the previous transfer */
  ((USBD_HID_HandleTypeDef *)pdev->pClassData)->state = HID_IDLE;
  return USBD_OK;
}


/**
* @brief  DeviceQualifierDescriptor 
*         return Device Qualifier descriptor
* @param  length : pointer data length
* @retval pointer to descriptor buffer
*/
static uint8_t  *USBD_HID_GetDeviceQualifierDesc (uint16_t *length)
{
  *length = sizeof (USBD_HID_DeviceQualifierDesc);
  return USBD_HID_DeviceQualifierDesc;
}

/**
  * @}
  */ 


/**
  * @}
  */ 


/**
  * @}
  */ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
