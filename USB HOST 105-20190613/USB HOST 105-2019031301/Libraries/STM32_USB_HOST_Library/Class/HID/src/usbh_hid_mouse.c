/**
  ******************************************************************************
  * @file    usbh_hid_mouse.c 
  * @author  MCD Application Team
  * @version V2.2.0
  * @date    09-November-2015
  * @brief   This file is the application layer for USB Host HID Mouse Handling.                  
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "usbh_hid_mouse.h"
#include <string.h>
#include "drv_spi.h"

extern u8 needsendusbdata;
extern uint8_t nrf24l01txbuffer[32];
extern uint16_t i;
extern uint8_t buffer[64];

extern char UsbBuf[1000];

extern char cConfDesc[0x100];
extern void My_Usart2_Send(char *string,uint16_t length);
extern void SendBleTxTouch(uint8_t com,uint16_t wLength,char *Data);
extern void SendBleTx(uint8_t com,uint16_t wLength,char *Data);
/** @addtogroup USBH_LIB
  * @{
  */

/** @addtogroup USBH_CLASS
  * @{
  */

/** @addtogroup USBH_HID_CLASS
  * @{
  */
  
/** @defgroup USBH_HID_MOUSE 
  * @brief    This file includes HID Layer Handlers for USB Host HID class.
  * @{
  */ 

/** @defgroup USBH_HID_MOUSE_Private_TypesDefinitions
  * @{
  */ 
/**
  * @}
  */ 


/** @defgroup USBH_HID_MOUSE_Private_Defines
  * @{
  */ 
/**
  * @}
  */ 


/** @defgroup USBH_HID_MOUSE_Private_Macros
  * @{
  */ 
/**
  * @}
  */ 

/** @defgroup USBH_HID_MOUSE_Private_FunctionPrototypes
  * @{
  */ 
static void  MOUSE_Init (void);
static void  MOUSE_Decode(uint8_t *data,uint8_t length);
/**
  * @}
  */ 


/** @defgroup USBH_HID_MOUSE_Private_Variables
  * @{
  */
#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
 #if defined   (__CC_ARM) /*!< ARM Compiler */
  __align(4) 
 #elif defined ( __ICCARM__ ) /*!< IAR Compiler */
  #pragma data_alignment=4
 #elif defined (__GNUC__) /*!< GNU Compiler */
 #pragma pack(4) 
 #elif defined  (__TASKING__) /*!< TASKING Compiler */                           
  __align(4) 
 #endif /* __CC_ARM */
#endif
 
 
HID_MOUSE_Data_TypeDef HID_MOUSE_Data;
HID_cb_TypeDef HID_MOUSE_cb = 
{
  MOUSE_Init,
  MOUSE_Decode,
};
/**
  * @}
  */ 


/** @defgroup USBH_HID_MOUSE_Private_Functions
  * @{
  */ 

/**
* @brief  MOUSE_Init
*         Init Mouse State.
* @param  None
* @retval None
*/
static void  MOUSE_Init ( void)
{
 /* Call User Init*/
 USR_MOUSE_Init();
}
      
extern void UART_DMA_TX(u32 Address,u16 count);
extern void My_Usart1_Send(char *string,uint8_t length);
extern uint8_t UartTxBuffer[2048];
extern uint16_t TouchReportTm;
/**
* @brief  MOUSE_Decode
*         Decode Mouse data
* @param  data : Pointer to Mouse HID data buffer
* @retval None
*/
extern uint16_t USBVid,USBPid;
uint8_t tstbuf[8],touchdatalen,touchnum;
static void  MOUSE_Decode(uint8_t *data,uint8_t length)
{
	if(length)
	{
		touchdatalen = length;
//		if(0)//USBVid == 0x222a)
//		{
//				uint8_t i,*pdata,*pbuffer,num,keyst;
//			  touchnum = data[55];
//				buffer[0] = 0x02;
//				pbuffer = &buffer[1];
//				pdata = &data[1];
//				for(i=0;i<10;i++)
//				{
//					if(i<touchnum)
//					{
//							keyst = *pdata++;
//							num = keyst & 0x3f;
//						  *pbuffer++ = num;
//							if((keyst & 0x40))
//								*pbuffer++ = 0x07;
//							else
//								*pbuffer++ = 0x04;
//							*pbuffer++ = *pdata++;	
//							*pbuffer++ = *pdata++;	
//							*pbuffer++ = *pdata++;	
//							*pbuffer++ = *pdata++;								
//					}
//					else
//					{
//							*pbuffer++ = 0;	
//							*pbuffer++ = 0;	
//							*pbuffer++ = 0;	
//							*pbuffer++ = 0;	
//							*pbuffer++ = 0;	
//							*pbuffer++ = 0;	
//						  pdata += 5;
//					}
//				}
//				//tstbuf[0]= *pdata++;
//				//tstbuf[1]= *pdata++;
//				//tstbuf[2]= *pdata++;
//				//tstbuf[3]= *pdata++;
//				//tstbuf[4]= *pdata++;
//				//tstbuf[5]= *pdata++;
//				pdata += 4;
//				*pbuffer = *pdata;	
//				touchdatalen = 62;
//		}
//		else
			memcpy(buffer,data,touchdatalen);//length);
			SendBleTx(0xB0,touchdatalen,(char *)buffer);
			TouchReportTm = 500;//300ms
////		if(cConfDesc[2]==0x3B)
////		{
////			SendBleTx(0xB0,touchdatalen,(char *)buffer);//十點length+5,(char *)buffer);//十點
////		}
////		else if(cConfDesc[2]==0x29)
////		{
////			SendBleTx(0xB0,64,(char *)buffer);//十點
////		}
////		else
////		{
////			SendBleTx(0xB0,64,(char *)buffer);//十點
////		}
		//if(buffer[61]!=0x0a)
//buffer[61]=0x0a;
		//memset(buffer,0x00,64);
#if 0 //20190108
		memcpy(buffer,data,length);
		//SendBleTx(0xB0,7,(char *)buffer);//單點7byte ,0123456 61
		//SendBleTx(0xB1,64,(char *)buffer);//單點7byte ,0123456 61
		SendBleTxTouch(0xB1,64,(char *)buffer);//十點
#endif
#if	0 //BLE	
		
		UsbBuf[0] = 0xBB;
		UsbBuf[1] = length;//資料長度
		
		for(i=0;i<length;i++)
		{
			UsbBuf[i+2] = buffer[i];
		}
	
		My_Usart2_Send(UsbBuf,length+2);
#endif		
#if	0 //mac
		nrf24l01txbuffer[0] = 0xB0 | buffer[61];//兩點觸控資料
		for(i=1;i<32;i++)
		{
			nrf24l01txbuffer[i] = buffer[s1++];
		}
		NRF24L01_TxPacket((uint8_t *)nrf24l01txbuffer);
#endif
#if 0		
		nrf24l01txbuffer[0] = 0xBB;
		nrf24l01txbuffer[1] = length;//資料長度
		for(i=2;i<32;i++)
		{
			nrf24l01txbuffer[i] = buffer[s1++];
			if(s1==length)
			{
				i=32;
			}
		}
		NRF24L01_TxPacket((uint8_t *)nrf24l01txbuffer);
		
		while(s1<length)
		{
			for(i=0;i<32;i++)
			{
				nrf24l01txbuffer[i] = buffer[s1++];
				if(s1==length)
				{
					i=32;
				}
			}
			NRF24L01_TxPacket((uint8_t *)nrf24l01txbuffer);
		}
#endif
	}
}
/**
  * @}
  */ 

/**
  * @}
  */ 

/**
  * @}
  */

/**
  * @}
  */


/**
  * @}
  */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
