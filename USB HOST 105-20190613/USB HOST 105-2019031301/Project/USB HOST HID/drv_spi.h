#ifndef __DRV_SPI_H__
#define __DRV_SPI_H__

#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_spi.h"

//SPI引腳定義
#define SPI_CLK_GPIO_PORT			GPIOA
#define SPI_CLK_GPIO_CLK			RCC_APB2Periph_GPIOA
#define SPI_CLK_GPIO_PIN			GPIO_Pin_5

#define SPI_MISO_GPIO_PORT			GPIOA
#define SPI_MISO_GPIO_CLK			RCC_APB2Periph_GPIOA
#define SPI_MISO_GPIO_PIN			GPIO_Pin_6

#define SPI_MOSI_GPIO_PORT			GPIOA
#define SPI_MOSI_GPIO_CLK			RCC_APB2Periph_GPIOA
#define SPI_MOSI_GPIO_PIN			GPIO_Pin_7

#define SPI_NSS_GPIO_PORT			GPIOA
#define SPI_NSS_GPIO_CLK			RCC_APB2Periph_GPIOA
#define SPI_NSS_GPIO_PIN			GPIO_Pin_4


#define spi_set_nss_high( )			SPI_NSS_GPIO_PORT->ODR |= SPI_NSS_GPIO_PIN								//片選置高
#define spi_set_nss_low( )			SPI_NSS_GPIO_PORT->ODR &= (uint32_t)( ~((uint32_t)SPI_NSS_GPIO_PIN ))	//片選置低

/** RF24L01硬件接口定義 */
#define RF24L01_CE_GPIO_PORT			GPIOA
#define RF24L01_CE_GPIO_CLK				RCC_APB2Periph_GPIOA
#define RF24L01_CE_GPIO_PIN				GPIO_Pin_2

#define RF24L01_IRQ_GPIO_PORT			GPIOA
#define RF24L01_IRQ_GPIO_CLK			RCC_APB2Periph_GPIOA
#define RF24L01_IRQ_GPIO_PIN			GPIO_Pin_3

#define RF24L01_CS_GPIO_PORT			SPI_NSS_GPIO_PORT			//PG7
#define RF24L01_CS_GPIO_CLK				SPI_NSS_GPIO_CLK
#define RF24L01_CS_GPIO_PIN				SPI_NSS_GPIO_PIN

/** 口線操作函數定義 */
#define NRF24L01_CE_HIGH( )			RF24L01_CE_GPIO_PORT->ODR |= RF24L01_CE_GPIO_PIN
#define NRF24L01_CE_LOW( )			RF24L01_CE_GPIO_PORT->ODR &= (uint32_t)( ~(uint32_t)RF24L01_CE_GPIO_PIN )

#define NRF24L01_SPI_CS_DISABLE( )			spi_set_nss_high( )
#define NRF24L01_SPI_CS_ENABLE( )			spi_set_nss_low( )

#define NRF24L01_IRQ_PIN_READ( )		(( RF24L01_IRQ_GPIO_PORT->IDR & (uint32_t)RF24L01_IRQ_GPIO_PIN) != RF24L01_IRQ_GPIO_PIN ) ? 0 : 1	//IRQ狀態


//SPI接口定義
#define SPI_PORT					SPI1						//SPI接口
#define SPI_PORT_CLK				RCC_APB2Periph_SPI1			//SPI時鐘

// NRF24L01發送接收數據寬度定義
#define TX_ADR_WIDTH                                  5   	//5字節的地址寬度
#define RX_ADR_WIDTH                                  5   	//5字節的地址寬度
#define TX_PLOAD_WIDTH                                32  	//32字節的用戶數據寬度
#define RX_PLOAD_WIDTH                                32  	//32字節的用戶數據寬度

//NRF24L01寄存器操作命令
#define NRF_READ_REG    0x00  //讀配置寄存器,低5位為寄存器地址
#define NRF_WRITE_REG   0x20  //寫配置寄存器,低5位為寄存器地址
#define RD_RX_PLOAD     0x61  //讀RX有效數據,1~32字節
#define WR_TX_PLOAD     0xA0  //寫TX有效數據,1~32字節
#define FLUSH_TX        0xE1  //清除TX FIFO寄存器.發射模式下用
#define FLUSH_RX        0xE2  //清除RX FIFO寄存器.接收模式下用
#define REUSE_TX_PL     0xE3  //重新使用上一包數據,CE為高,數據包被不斷發送.
#define NOP             0xFF  //空操作,可以用來讀狀態寄存器	 
//SPI(NRF24L01)寄存器地址
#define CONFIG          0x00  //配置寄存器地址;bit0:1接收模式,0發射模式;bit1:電選擇;bit2:CRC模式;bit3:CRC使能;
                              //bit4:中斷MAX_RT(達到最大重發次數中斷)使能;bit5:中斷TX_DS使能;bit6:中斷RX_DR使能
#define EN_AA           0x01  //使能自動應答功能  bit0~5,對應通道0~5
#define EN_RXADDR       0x02  //接收地址允許,bit0~5,對應通道0~5
#define SETUP_AW        0x03  //設置地址寬度(所有數據通道):bit1,0:00,3字節;01,4字節;02,5字節;
#define SETUP_RETR      0x04  //建立自動重發;bit3:0,自動重發計數器;bit7:4,自動重發延時 250*x+86us
#define RF_CH           0x05  //RF通道,bit6:0,工作通道頻率;
#define RF_SETUP        0x06  //RF寄存器;bit3:傳輸速率(0:1Mbps,1:2Mbps);bit2:1,發射功率;bit0:低噪聲放大器增益
#define STATUS          0x07  //狀態寄存器;bit0:TX FIFO滿標誌;bit3:1,接收數據通道號(最大:6);bit4,達到最多次重發
                              //bit5:數據發送完成中斷;bit6:接收數據中斷;
#define MAX_TX  		    0x10  //達到最大發送次數中斷
#define TX_OK   		    0x20  //TX發送完成中斷
#define RX_OK   		    0x40  //接收到數據中斷

#define OBSERVE_TX      0x08  //發送檢測寄存器,bit7:4,數據包丟失計數器;bit3:0,重發計數器
#define CD              0x09  //載波檢測寄存器,bit0,載波檢測;
#define RX_ADDR_P0      0x0A  //數據通道0接收地址,最大長度5個字節,低字節在前
#define RX_ADDR_P1      0x0B  //數據通道1接收地址,最大長度5個字節,低字節在前
#define RX_ADDR_P2      0x0C  //數據通道2接收地址,最低字節可設置,高字節,必須同RX_ADDR_P1[39:8]相等;
#define RX_ADDR_P3      0x0D  //數據通道3接收地址,最低字節可設置,高字節,必須同RX_ADDR_P1[39:8]相等;
#define RX_ADDR_P4      0x0E  //數據通道4接收地址,最低字節可設置,高字節,必須同RX_ADDR_P1[39:8]相等;
#define RX_ADDR_P5      0x0F  //數據通道5接收地址,最低字節可設置,高字節,必須同RX_ADDR_P1[39:8]相等;
#define TX_ADDR         0x10  //發送地址(低字節在前),ShockBurstTM模式下,RX_ADDR_P0與此地址相等
#define RX_PW_P0        0x11  //接收數據通道0有效數據寬度(1~32字節),設置為0則非法
#define RX_PW_P1        0x12  //接收數據通道1有效數據寬度(1~32字節),設置為0則非法
#define RX_PW_P2        0x13  //接收數據通道2有效數據寬度(1~32字節),設置為0則非法
#define RX_PW_P3        0x14  //接收數據通道3有效數據寬度(1~32字節),設置為0則非法
#define RX_PW_P4        0x15  //接收數據通道4有效數據寬度(1~32字節),設置為0則非法
#define RX_PW_P5        0x16  //接收數據通道5有效數據寬度(1~32字節),設置為0則非法
#define NRF_FIFO_STATUS 0x17  //FIFO狀態寄存器;bit0,RX FIFO寄存器空標誌;bit1,RX FIFO滿標誌;bit2,3,保留
                              //bit4,TX FIFO空標誌;bit5,TX FIFO滿標誌;bit6,1,循環發送上一數據包.0,不循環;
       


void MX_SPI1_Init(void);
uint8_t drv_spi_read_write_byte( uint8_t TxByte );
void drv_spi_read_write_string( uint8_t* ReadBuffer, uint8_t* WriteBuffer, uint16_t Length );
uint8_t NRF24L01_check( void );
uint8_t NRF24L01_Write_Buf(uint8_t reg, uint8_t *pBuf, uint8_t len);
uint8_t NRF24L01_Write_Reg(uint8_t reg,uint8_t value);
uint8_t NRF24L01_Read_Reg(uint8_t reg);
uint8_t NRF24L01_Read_Buf(uint8_t reg,uint8_t *pBuf,uint8_t len);
uint8_t NRF24L01_Write_Buf(uint8_t reg, uint8_t *pBuf, uint8_t len);
uint8_t NRF24L01_TxPacket(uint8_t *txbuf);
uint8_t NRF24L01_RxPacket(uint8_t *rxbuf);
void NRF24L01_RX_Mode(void);
void NRF24L01_TX_Mode(void);
#endif

