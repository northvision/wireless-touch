#include "drv_spi.h"
#include "drv_delay.h"

#define SPI_WAIT_TIMEOUT			((uint16_t)0xFFFF)

const char *g_ErrorString = "RF24L01 is not find !...";

const uint8_t TX_ADDRESS[TX_ADR_WIDTH]={0xb0,0x43,0x10,0x10,0x01}; //發送地址
const uint8_t RX_ADDRESS[RX_ADR_WIDTH]={0xb0,0x43,0x10,0x10,0x01}; //接收地址

extern void My_Usart1_Send(char *string,uint8_t length);

/**
  * @brief :SPI Init(HW)
  * @param :none
  * @note  :none
  * @retval:none
  */ 
void MX_SPI1_Init(void)
{
	GPIO_InitTypeDef	SpiGpioInitStructer;
	SPI_InitTypeDef		SpiInitStructer;
	
	/** SPI引腳配置 */
	RCC_APB2PeriphClockCmd( SPI_CLK_GPIO_CLK | SPI_MISO_GPIO_CLK | SPI_MOSI_GPIO_CLK | SPI_NSS_GPIO_CLK, ENABLE );	//打開端口時鐘
	
	//SCK MOSI MISO 配置為復用
	SpiGpioInitStructer.GPIO_Speed = GPIO_Speed_10MHz;
	SpiGpioInitStructer.GPIO_Mode = GPIO_Mode_AF_PP;
	
	SpiGpioInitStructer.GPIO_Pin = SPI_CLK_GPIO_PIN;
	GPIO_Init( SPI_CLK_GPIO_PORT, &SpiGpioInitStructer );		//初始化SCK
	
	SpiGpioInitStructer.GPIO_Pin = SPI_MOSI_GPIO_PIN;
	GPIO_Init( SPI_MOSI_GPIO_PORT, &SpiGpioInitStructer );		//初始化MOSI
	
	SpiGpioInitStructer.GPIO_Pin = SPI_MISO_GPIO_PIN;
	GPIO_Init( SPI_MISO_GPIO_PORT, &SpiGpioInitStructer );		//初始化MISO
	
	//NSS配置為推輓輸出
	SpiGpioInitStructer.GPIO_Mode = GPIO_Mode_Out_PP;
	SpiGpioInitStructer.GPIO_Pin = SPI_NSS_GPIO_PIN;
	GPIO_Init( SPI_NSS_GPIO_PORT, &SpiGpioInitStructer );		//初始化NSS
	GPIO_SetBits( SPI_NSS_GPIO_PORT, SPI_NSS_GPIO_PIN );		//置高

	/** SPI配置 */
	SPI_I2S_DeInit( SPI_PORT );			//復位SPI
	
	if( SPI1 == SPI_PORT )				
	{
		RCC_APB2PeriphClockCmd( SPI_PORT_CLK, ENABLE );			//SPI1在APB2上，打開相應SPI時鐘
	}
	else
	{
		RCC_APB1PeriphClockCmd( SPI_PORT_CLK, ENABLE );			//SPI2 3在APB1上
	}
	
	SPI_Cmd( SPI_PORT, DISABLE );		//關閉SPI外設，配置前關閉
	
	SpiInitStructer.SPI_Direction = SPI_Direction_2Lines_FullDuplex;	//雙線全雙工
	SpiInitStructer.SPI_Mode = SPI_Mode_Master;							//主機模式
	SpiInitStructer.SPI_CPOL = SPI_CPOL_Low;							//空閒狀態為低電平 
	SpiInitStructer.SPI_CPHA = SPI_CPHA_1Edge;							//第一個邊沿採集數據
	SpiInitStructer.SPI_DataSize = SPI_DataSize_8b;						//8位數據
	SpiInitStructer.SPI_NSS = SPI_NSS_Soft;								//從機軟件管理
	SpiInitStructer.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_64;	//32分頻
	SpiInitStructer.SPI_FirstBit = SPI_FirstBit_MSB;					//最高位先發送
	SpiInitStructer.SPI_CRCPolynomial = 7;								//CRC多項式,默認不使用SPI自帶CRC	 
	
	SPI_Init( SPI_PORT, &SpiInitStructer );
	SPI_Cmd( SPI_PORT, ENABLE );
}

/**
  * @brief :SPI收發一個字節
  * @param :
  *			@TxByte: 發送的數據字節
  * @note  :非堵塞式，一旦等待超時，函數會自動退出
  * @retval:接收到的字節
  */
uint8_t drv_spi_read_write_byte( uint8_t TxByte )
{
	uint8_t l_Data = 0;
	uint16_t l_WaitTime = 0;
	
	while( RESET == SPI_I2S_GetFlagStatus( SPI_PORT, SPI_I2S_FLAG_TXE ) )		//等待發送緩衝區為空
	{
		if( SPI_WAIT_TIMEOUT == ++l_WaitTime )
		{
			break;			//如果等待超時則退出
		}
	}
	l_WaitTime = SPI_WAIT_TIMEOUT / 2;		//重新設置接收等待時間(因為SPI的速度很快，正常情況下在發送完成之後會立即收到數據，等待時間不需要過長)
	SPI_PORT->DR = TxByte;	//發送數據
	
	while( RESET == SPI_I2S_GetFlagStatus( SPI_PORT, SPI_I2S_FLAG_RXNE ) )		//等待接收緩衝區非空
	{
		if( SPI_WAIT_TIMEOUT == ++l_WaitTime )
		{
			break;			//如果等待超時則退出
		}
	}
	
	l_Data = (uint8_t)SPI_PORT->DR;		//讀取接收數據
	
	return l_Data;		//返回
}

/**
  * 函數功能 : 檢測24L01是否存在
  * 輸入參數 : 無
  * 返回值   : 0:成功;1:失敗
  * 說明	 : 無
  */ 
uint8_t NRF24L01_check( void )
{
	uint8_t i;
	uint8_t buf[5]={ 0XA5, 0XA5, 0XA5, 0XA5, 0XA5 };
	
	NRF24L01_Write_Buf( NRF_WRITE_REG+TX_ADDR, buf, 5 );			//寫入5個字節的地址
	NRF24L01_Read_Buf( TX_ADDR, buf, 5 );							//讀出寫入的地址
	
	for(i=0;i<5;i++)if(buf[i]!=0XA5)break;
	if(i!=5)return 1;												//檢測24L01錯誤
	return 0;														//檢測到24L01
}

/**
  * 函數功能 : 寫入SPI寄存器
  * 輸入參數 : 無
  * 返回值   : 無
  * 說明	 : reg:指定寄存器地址
  *           
  */ 
uint8_t NRF24L01_Write_Reg(uint8_t reg,uint8_t value)
{
	uint8_t status;	
	NRF24L01_SPI_CS_ENABLE();                 						//使能SPI傳輸
	status =drv_spi_read_write_byte(reg);							//發送寄存器值
	drv_spi_read_write_byte(value);      							//寫入寄存器值
	NRF24L01_SPI_CS_DISABLE();                 						//關閉SPI傳輸   
	return(status);       											//返回讀到的狀態值
}

/**
  * 函數功能 : 讀取SPI寄存器
  * 輸入參數 : 無
  * 返回值   : 無
  * 說明	 : reg:要讀的寄存器地址
  *           
  */ 
uint8_t NRF24L01_Read_Reg(uint8_t reg)
{
	uint8_t reg_val;	    
	NRF24L01_SPI_CS_ENABLE();          								//使能SPI傳輸		
	drv_spi_read_write_byte(reg);									//發送寄存器值
	reg_val=drv_spi_read_write_byte(0XFF);							//讀取寄存器的值
	NRF24L01_SPI_CS_DISABLE();										//關閉SPI傳輸   	    
	return(reg_val);												//返回讀到的狀態值
}

/**
  * 函數功能 : 在指定位置讀出指定長度的數據
  * 輸入參數 : 無
  * 返回值   : 此次讀到的狀態寄存器的值
  * 說明	 : 無
  */ 
uint8_t NRF24L01_Read_Buf(uint8_t reg,uint8_t *pBuf,uint8_t len)
{
	uint8_t status,uint8_t_ctr;	   
  
	NRF24L01_SPI_CS_ENABLE();          								//使能SPI傳輸
	status=drv_spi_read_write_byte(reg);							//發送寄存器值(位置),並讀取狀態值
 	for(uint8_t_ctr=0;uint8_t_ctr<len;uint8_t_ctr++)
	{
		pBuf[uint8_t_ctr]=drv_spi_read_write_byte(0XFF);			//讀出數據
	}
	NRF24L01_SPI_CS_DISABLE();       								//關閉SPI傳輸
	return status;        											//返回讀到的狀態值
}	
/**
  * 函數功能 : 在指定位置寫指定長度的數據
  * 輸入參數 : 無
  * 返回值   : 無
  * 說明	 : reg:寄存器(位置)  *pBuf:數據指標  Len:數據長度
  */ 
uint8_t NRF24L01_Write_Buf(uint8_t reg, uint8_t *pBuf, uint8_t len)
{
	uint8_t status,uint8_t_ctr;	    
 	NRF24L01_SPI_CS_ENABLE();          								//使能SPI傳輸
	status = drv_spi_read_write_byte(reg);							//發送寄存器值(位置),並讀取狀態值
	for(uint8_t_ctr=0; uint8_t_ctr<len; uint8_t_ctr++)
	{
		drv_spi_read_write_byte(*pBuf++); 							//寫入數據	
	}
	NRF24L01_SPI_CS_DISABLE();       								//關閉SPI傳輸
	return status;          										//返回讀到的狀態值
}

/**
  * 函數功能 : 啟動NRF24L01發送一次數據
  * 輸入參數 : 無
  * 返回值   : 發送完成狀況
  * 說明	 : txbuf:待發送數據首地址
  *           
  */ 
uint8_t NRF24L01_TxPacket(uint8_t *txbuf)
{
	uint8_t sta;
	NRF24L01_CE_LOW();
	NRF24L01_Write_Buf(WR_TX_PLOAD,txbuf,TX_PLOAD_WIDTH);			//寫數據到TX BUF 32個字節
 	NRF24L01_CE_HIGH();												//啟動發送
  
	while(NRF24L01_IRQ_PIN_READ()!=0);								//等待發送完成
  
	sta=NRF24L01_Read_Reg(STATUS);									//讀取狀態寄存器的值
	NRF24L01_Write_Reg(NRF_WRITE_REG+STATUS,sta);					//清除TX_DS或MAX_RT中斷標誌
	
	//delay(2);
	if(sta&MAX_TX)													//達到最大重發次數
	{
		NRF24L01_Write_Reg(FLUSH_TX,0xff);							//清除TX_FIFO寄存器 
		return MAX_TX; 
	}
	if(sta&TX_OK)													//發送完成
	{
		return TX_OK;
	}
	return 0xff;													//其它原因發送失敗
}

/**
  * 函數功能 : 啟動NRF24L01接收一次數據
  * 輸入參數 : 無
  * 返回值   : 接收完成狀況
  * 說明	 : 無
  *           
  */ 
uint8_t NRF24L01_RxPacket(uint8_t *rxbuf)
{
	uint8_t sta;		    							   
	sta=NRF24L01_Read_Reg(STATUS);  								//讀取狀態寄存器的值 
	NRF24L01_Write_Reg(NRF_WRITE_REG+STATUS,sta); 					//清除TX_DS或MAX_RT中斷標誌
	if(sta&RX_OK)													//接收到的數據
	{
		NRF24L01_Read_Buf(RD_RX_PLOAD,rxbuf,RX_PLOAD_WIDTH);		//讀取數據
		NRF24L01_Write_Reg(FLUSH_RX,0xff);							//清除RX_FIFO寄存器 
		return 0; 
	}	   
	return 1;														//沒收到任何數據
}					    

/**
  * 函數功能 : 將函數初始化NRF24L01到RX模式
  * 輸入參數 : 無
  * 返回值   : 無
  * 說明	 : 無
  *           
  */ 
void NRF24L01_RX_Mode(void)
{
	NRF24L01_CE_LOW();	  
	NRF24L01_Write_Reg(NRF_WRITE_REG+CONFIG, 0x0F);					//配置基本工作模式的參數;PWR_UP,EN_CRC,16BIT_CRC 
	NRF24L01_Write_Reg(NRF_WRITE_REG+EN_AA,0x01);					//使能通道0的自動應答
	NRF24L01_Write_Reg(NRF_WRITE_REG+EN_RXADDR,0x01);				//使能通道0的接收地址
	NRF24L01_Write_Reg(NRF_WRITE_REG+RF_CH,50);						//設置RF通信頻率
	NRF24L01_Write_Reg(NRF_WRITE_REG+RF_SETUP,0x0f);				//設置TX發射參數,0db增益,2Mbps,低噪聲增益開啟
  
	NRF24L01_Write_Reg(NRF_WRITE_REG+RX_PW_P0,RX_PLOAD_WIDTH);		//選擇通道0的有效數據長度
    
	NRF24L01_Write_Buf(NRF_WRITE_REG+RX_ADDR_P0,(uint8_t*)RX_ADDRESS,RX_ADR_WIDTH);// RX點地址	
	  
  
	NRF24L01_CE_HIGH(); 											//CE為高,進入接收模式
//	drv_delay_ms(1);
}						 

/**
  * 函數功能 : 將函數初始化NRF24L01到TX模式
  * 輸入參數 : 無
  * 返回值   : 無
  * 說明	 : 無
  *           
  */ 
void NRF24L01_TX_Mode(void)
{														 
	NRF24L01_CE_LOW();	    
	NRF24L01_Write_Buf(NRF_WRITE_REG+TX_ADDR,(uint8_t*)TX_ADDRESS,TX_ADR_WIDTH);	//TX點地址
	NRF24L01_Write_Buf(NRF_WRITE_REG+RX_ADDR_P0,(uint8_t*)RX_ADDRESS,RX_ADR_WIDTH); //設置TX點地址,主要為了使能ACK

	NRF24L01_Write_Reg(NRF_WRITE_REG+EN_AA,0x01);     								//使能通道0的自動應答    
	NRF24L01_Write_Reg(NRF_WRITE_REG+EN_RXADDR,0x01); 								//使能通道0的接收地址
	NRF24L01_Write_Reg(NRF_WRITE_REG+SETUP_RETR,0xff);								//設置自動重發間隔時間:4000us + 86us;最大自動重發次數15次
	NRF24L01_Write_Reg(NRF_WRITE_REG+RF_CH,50);       								//設置RF通道為40
	NRF24L01_Write_Reg(NRF_WRITE_REG+RF_SETUP,0x0f);  								//設置TX發射參數,0db增益,2Mbps,低噪聲增益開啟  
	NRF24L01_Write_Reg(NRF_WRITE_REG+CONFIG,0x0e);    								//配置基本工作模式的參數:PWR_UP,EN_CRC,16BIT_CRC,接收模式,開啟所有中斷
	NRF24L01_CE_HIGH();																//CE為高,10us後啟動發送
//	drv_delay_ms(1);
}




