/**
  ******************************************************************************
  * @file    USART/DMA_Polling/main.c 
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "platform_config.h"
#include "usb_bsp.h"
#include "usbh_core.h"
#include "usbh_usr.h"
#include "usbh_hid_core.h"
#include "HIDParser.h"

#include "drv_spi.h"
#include "drv_delay.h"

/** @addtogroup STM32F10x_StdPeriph_Examples
  * @{
  */

/** @addtogroup USART_DMA_Polling
  * @{
  */ 
	
#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
  #if defined ( __ICCARM__ ) /*!< IAR Compiler */
    #pragma data_alignment=4   
  #endif
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */
__ALIGN_BEGIN USB_OTG_CORE_HANDLE           USB_OTG_Core_dev __ALIGN_END ;

#ifdef USB_OTG_HS_INTERNAL_DMA_ENABLED
  #if defined ( __ICCARM__ ) /*!< IAR Compiler */
    #pragma data_alignment=4   
  #endif
#endif /* USB_OTG_HS_INTERNAL_DMA_ENABLED */
__ALIGN_BEGIN USBH_HOST                     USB_Host __ALIGN_END ;	

/* Private typedef -----------------------------------------------------------*/
typedef enum { FAILED = 0, PASSED = !FAILED} TestStatus;

/* Private define ------------------------------------------------------------*/
#define TxBufferSize   (countof(UartTxBuffer) - 1)
//#define TxBufferSize2   (countof(TxBuffer2) - 1)

/* Private macro -------------------------------------------------------------*/
#define countof(a)   (sizeof(a) / sizeof(*(a)))

/* Private variables ---------------------------------------------------------*/
USART_InitTypeDef USART_InitStructure;
uint8_t UartTxBuffer[512];
//#pragma data_alignment=4    
uint8_t Uart2TxBuffer[256]; 
uint16_t TouchReportInterfaceNum;
uint16_t USBVid,USBPid;
u16 TouchReportInterfaceNum;
uint8_t test_f,TouchPointMaxNum,TouchReportID,TouchCountMaxID,TouchDeviceSettingID,HIDinterfaceNumber,HIDIntFaceTmp;
HID_ReportInfo_t ParserData;
HID_ReportItem_t *pHIDReportItemBegin;

volatile TestStatus TransferStatus1 = FAILED;
volatile TestStatus TransferStatus2 = FAILED; 

volatile uint16_t TouchReportTm;
volatile uint32_t TouchReportEnd_f;

uint16_t i;
char nrf24l01txbuffer[32];
char nrf24l01rxbuffer[32];
char needsendusbdata=0;
char cDevDesc[18];
char cConfDesc[0x100];
char cManufacturerStrDesc[256];
char cProductStrDesc[256];
char cSerialNumberStrDesc[256];
char cReportDesc[1000] = {0};
unsigned short cReprotDescLength,usCount;

uint8_t buffer[64] = {0x02,
											0x07,0x01,0x00,0x05,0x10,0x10,
											0x07,0x02,0x00,0x0A,0x10,0x10,
											0x07,0x03,0x00,0x0F,0x10,0x10,
											0x07,0x04,0x00,0x15,0x10,0x10,
											0x07,0x05,0x00,0x1A,0x10,0x10,
											0x07,0x06,0x00,0x1F,0x10,0x10,
											0x07,0x07,0x00,0x25,0x10,0x10,
											0x07,0x08,0x00,0x2A,0x10,0x10,
											0x07,0x09,0x00,0x2F,0x10,0x10,
											0x07,0x0A,0x00,0x35,0x10,0x10,
											0x0A
};

#define cc2541 1
#define Rxbufsize	200
#define Txbufsize 1000
static void MX_USART1_UART_Init(void);
static void MX_USART2_UART_Init(uint32_t baud);
void My_Usart1_Send(char *string,uint16_t length);
void My_Usart2_Send(char *string,uint16_t length);
char aRx1buf[1];
char aRx2buf[1];
char Rx1buf[Rxbufsize];
char Rx2buf[Rxbufsize];
char Tx1buf[Txbufsize];
char Tx2buf[Txbufsize];
uint8_t Rx1count=0;
uint8_t Rx2count=0;
uint8_t Tx1count=0;
uint8_t Tx2count=0;
uint16_t Tx1CounterSize=0;
uint16_t Tx2CounterSize=0;
//char UsbBuf[1000];
void SendBleTx(uint8_t com,uint16_t wLength,char *Data);
void SendBleTxTouch(uint8_t com,uint16_t wLength,char *Data);
/* Private function prototypes -----------------------------------------------*/
void RCC_Configuration(void);
void GPIO_Configuration(void);
void DMA_Configuration(void);
void UART_DMA_TX(u32 Address,u16 count);
void UART2_DMA_TX(u32 Address,u16 count);
TestStatus Buffercmp(uint8_t* pBuffer1, uint8_t* pBuffer2, uint16_t BufferLength);

//void uart_init(u32 bound);
void SendDevDesc(void);
void SendConfDesc(void);
void SendManufacturerStrDesc(void);
void SendProductStrDesc(void);
void SendSerialNumberStrDesc(void);
void SendcReportDesc1(void);
void SendcReportDesc2(void);
void SendcReportDesc3(void);
void SendcReportDesc4(void);
void SendcReportDesc5(void);
void SysTick_Init(void);
void SysTick1ms(void);
uint8_t time1msFlag=0;
void delay_ms(uint16_t nms);
uint16_t delayTime=0;

uint32_t time = 0;
uint16_t keycount=0;
uint8_t nextsend=0;
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
  /*!< At this stage the microcontroller clock setting is already configured, 
       this is done through SystemInit() function which is called from startup
       file (startup_stm32f10x_xx.s) before to branch to application main.
       To reconfigure the default setting of SystemInit() function, refer to
       system_stm32f10x.c file
     */     
       
  /* System Clocks Configuration */
  RCC_Configuration();
	
  SysTick_Init();	
  /* Configure the GPIO ports */
  GPIO_Configuration();

  /* Configure the DMA */
  DMA_Configuration();

/* USART4 and USARTz configuration ------------------------------------------------------*/
  /* USART4 and USARTz configured as follow:
        - BaudRate = 230400 baud  
        - Word Length = 8 Bits
        - One Stop Bit
        - No parity
        - Hardware flow control disabled (RTS and CTS signals)
        - Receive and transmit enabled
  */
  USART_InitStructure.USART_BaudRate = 115200;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  
  /* Configure USART4 */
  USART_Init(USART4, &USART_InitStructure);
  /* Enable USART4 DMA Rx and TX request */
  USART_DMACmd(USART4, USART_DMAReq_Tx, ENABLE);
  /* Enable USART4 TX DMA1 Channel */
  DMA_Cmd(USART4_Tx_DMA_Channel, DISABLE);
  /* Enable the USART4 */
  USART_Cmd(USART4, ENABLE);
	//USART_Cmd(USART4, DISABLE);
	
	#if cc2541
	MX_USART1_UART_Init();
	///delay_ms(500);
	MX_USART2_UART_Init(115200);
	///My_Usart2_Send("@baudrate,1152#",15);
	///delay_ms(500);
	///MX_USART2_UART_Init(115200);
	#else 
	//uart_init(115200);
	//MX_SPI1_Init();
	drv_delay_init();
	//My_Usart1_Send("NRF24L01 2.4G\n",14);
	
	while(NRF24L01_check())
	{
		//My_Usart1_Send("1",1);
		drv_delay_500Ms(2);
	}
	//My_Usart1_Send("0",1);
    NRF24L01_TX_Mode();		//Tx Mode
	#endif
	
	TouchReportInterfaceNum = 0;
	/* Init Host Library */
  USBH_Init(&USB_OTG_Core_dev, 
#ifdef USE_USB_OTG_FS
            USB_OTG_FS_CORE_ID,
#else
            USB_OTG_HS_CORE_ID,
#endif
            &USB_Host,
            &HID_cb, 
            &USR_Callbacks);
  while (1)
  {
			/* Host Task handler */
		USBH_Process(&USB_OTG_Core_dev , &USB_Host);
		//if(needsendusbdata>0)
		//{
			if(Tx2CounterSize == 0)
			{
				switch(needsendusbdata)
				{
					case 0:
						if(TouchReportEnd_f)
						{
								TouchReportEnd_f=0;
								while(DMA1_Channel7->CNDTR!=0);
								__disable_irq();
								Uart2TxBuffer[0] = 0xF1;
								Uart2TxBuffer[1] = 0xA5;
								Uart2TxBuffer[2] = 0x5A;
								Uart2TxBuffer[3] = 0;
								Uart2TxBuffer[4] = 5;
								Uart2TxBuffer[5] = 0xB1;
								Uart2TxBuffer[6] = 0xa5;
								Uart2TxBuffer[7] = 0x5a;
								Uart2TxBuffer[8] = 0xaa;
								Uart2TxBuffer[9] = 0x55;
								UART2_DMA_TX((u32)Uart2TxBuffer,10);
								__enable_irq();
								//delay_ms(50);
						}
						break;
					
					case 1:
						//memset(Uart2TxBuffer,0x00,200);
						while(DMA1_Channel7->CNDTR!=0);
						__disable_irq();
						Uart2TxBuffer[0] = 0xF1;
						Uart2TxBuffer[1] = 0xA5;
						Uart2TxBuffer[2] = 0x5A;
						Uart2TxBuffer[3] = 0;
						Uart2TxBuffer[4] = 3;
						Uart2TxBuffer[5] = 0xA0;
						Uart2TxBuffer[6] = cConfDesc[26];
						Uart2TxBuffer[7] = cConfDesc[25];
						UART2_DMA_TX((u32)Uart2TxBuffer,8);
						__enable_irq();
						delay_ms(50);
						needsendusbdata = 6;
						break;
					case 2:
						SendConfDesc();delay_ms(500);
						needsendusbdata++;
						break;
					case 3:
						SendManufacturerStrDesc();delay_ms(500);
						needsendusbdata++;
						break;
					case 4:
						SendProductStrDesc();delay_ms(500);
						needsendusbdata++;
						break;
					case 5:
						SendSerialNumberStrDesc();delay_ms(500);
						needsendusbdata++;
						break;
					case 6:
						SendcReportDesc1();delay_ms(50);
						needsendusbdata++;
						break;
					case 7:
						SendcReportDesc2();delay_ms(50);
						needsendusbdata++;
						break;
					case 8:
						SendcReportDesc3();delay_ms(50);
						needsendusbdata++;
						break;
					case 9:
						SendcReportDesc4();delay_ms(50);
						needsendusbdata++;
						break;
					case 10:
						SendcReportDesc5();delay_ms(50);
						needsendusbdata = 0;
						break;
				}
#if 0//20190108				
				switch(needsendusbdata)
				{
					case 1:
						SendDevDesc();delay_ms(500);
						needsendusbdata++;
						break;
					case 2:
						SendConfDesc();delay_ms(500);
						needsendusbdata++;
						break;
					case 3:
						SendManufacturerStrDesc();delay_ms(500);
						needsendusbdata++;
						break;
					case 4:
						SendProductStrDesc();delay_ms(500);
						needsendusbdata++;
						break;
					case 5:
						SendSerialNumberStrDesc();delay_ms(500);
						needsendusbdata++;
						break;
					case 6:
						SendcReportDesc1();delay_ms(500);
						needsendusbdata++;
						break;
					case 7:
						SendcReportDesc2();delay_ms(500);
						needsendusbdata++;
						break;
					case 8:
						SendcReportDesc3();delay_ms(500);
						needsendusbdata++;
						break;
					case 9:
						SendcReportDesc4();delay_ms(500);
						needsendusbdata++;
						break;
					case 10:
						SendcReportDesc5();delay_ms(500);
						needsendusbdata = 0;
						break;
				}
				#endif
			}		
		//}

			if(time1msFlag>0)
			{
				time1msFlag = 0;
				if(GPIO_ReadInputDataBit( GPIOC, GPIO_Pin_1)==0)
				{
					if(keycount<20)
					{
						keycount++;
					}
				}
				else
				{
					if(keycount>19)
					{
//						needsendusbdata = 1;
					}
					keycount = 0;
				}
			}
  }
}

void SendDevDesc(void)
{
	SendBleTx(0xA0,18,cDevDesc);
}

void SendConfDesc(void)
{
	uint16_t wTotalLength=0;
	
	wTotalLength = ((uint16_t)cConfDesc[3]<<8)+cConfDesc[2];
	
	SendBleTx(0xA1,wTotalLength,cConfDesc);
}

void SendManufacturerStrDesc(void)
{
	SendBleTx(0xA2,cManufacturerStrDesc[0],cManufacturerStrDesc);
}

void SendProductStrDesc(void)
{
	SendBleTx(0xA3,cProductStrDesc[0],cProductStrDesc);
}

void SendSerialNumberStrDesc(void)
{
	SendBleTx(0xA4,cSerialNumberStrDesc[0],cSerialNumberStrDesc);
}

void SendcReportDesc1(void)
{
	SendBleTx(0xA5,200,cReportDesc);
}

void SendcReportDesc2(void)
{
	SendBleTx(0xA6,200,cReportDesc+200);
}

void SendcReportDesc3(void)
{
	SendBleTx(0xA7,200,cReportDesc+400);
}

void SendcReportDesc4(void)
{
	SendBleTx(0xA8,200,cReportDesc+600);
}

void SendcReportDesc5(void)
{
	SendBleTx(0xA9,200,cReportDesc+800);
}

void SendBleTx(uint8_t com,uint16_t wLength,char *Data)
{
	//memset(UsbBuf,0x00,200);
	uint16_t len;
	len = wLength+1;
	while(DMA1_Channel7->CNDTR!=0);
	__disable_irq();
	Uart2TxBuffer[0] = 0xF1;
	Uart2TxBuffer[1] = 0xA5;
	Uart2TxBuffer[2] = 0x5A;
	Uart2TxBuffer[3] = (char)(len>>8);
	Uart2TxBuffer[4] = (char)len;
	Uart2TxBuffer[5] = com;
	
	for(i=0;i<wLength;i++)
	{
		Uart2TxBuffer[i+6] = *Data++;
	}
	
	//My_Usart2_Send(UsbBuf,wLength+5);
	
	UART2_DMA_TX((u32)Uart2TxBuffer,wLength+6);
	__enable_irq();
}


void SendBleTxTouch(uint8_t com,uint16_t wLength,char *Data)
{
	//memset(Uart2TxBuffer,0x00,200);
	uint16_t len;
	len = wLength+1;
	while(DMA1_Channel7->CNDTR!=0);
	Uart2TxBuffer[0] = 0xF1;
	Uart2TxBuffer[1] = 0xA5;
	Uart2TxBuffer[2] = 0x5A;
	Uart2TxBuffer[3] = (char)(len>>8);
	Uart2TxBuffer[4] = (char)len;
	Uart2TxBuffer[5] = com;
	
	for(i=0;i<wLength;i++)
	{
		Uart2TxBuffer[i+6] = *Data++;
	}
	
	//My_Usart2_Send(UsbBuf,wLength+3);
	UART2_DMA_TX((u32)Uart2TxBuffer,wLength+6);
}

void SysTick_Init(void)
{
	RCC_ClocksTypeDef RCC_ClocksStructure;
	RCC_GetClocksFreq(&RCC_ClocksStructure);
	
	/*set 1 sec tick 1000times*/
	SysTick_Config(RCC_ClocksStructure.SYSCLK_Frequency / 1000);
}

void SysTick1ms(void)
{
	time1msFlag = 1;
	if(delayTime>0)
	{
		delayTime--;
	}
}

void delay_ms(uint16_t nms)
{
	delayTime = nms;
	while(delayTime);
}
/**
  * @brief  Configures the different system clocks.
  * @param  None
  * @retval None
  */
void RCC_Configuration(void)
{  
  /* DMA clock enable */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1|RCC_AHBPeriph_DMA2, ENABLE);

  /* Enable GPIO clock */
  RCC_APB2PeriphClockCmd(USART4_GPIO_CLK | RCC_APB2Periph_AFIO, ENABLE);

#ifndef USE_STM3210C_EVAL
  /* Enable USART4 Clock */
  RCC_APB1PeriphClockCmd(USART4_CLK, ENABLE); 
#else
  /* Enable USART4 Clock */
  RCC_APB1PeriphClockCmd(USART4_CLK, ENABLE); 
#endif
  /* Enable USARTz Clock */
//  RCC_APB1PeriphClockCmd(USARTz_CLK, ENABLE);  
}

/**
  * @brief  Configures the different GPIO ports.
  * @param  None
  * @retval None
  */
void GPIO_Configuration(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);   
#ifdef USE_STM3210C_EVAL
  /* Enable the USART3 Pins Software Remapping */
  //GPIO_PinRemapConfig(GPIO_PartialRemap_USART3, ENABLE);
  
  /* Enable the USART2 Pins Software Remapping */
  //GPIO_PinRemapConfig(GPIO_Remap_USART2, ENABLE);  
#elif defined(USE_STM3210B_EVAL) || defined(USE_STM32100B_EVAL)
  /* Enable the USART2 Pins Software Remapping */
  GPIO_PinRemapConfig(GPIO_Remap_USART2, ENABLE);
#endif

  /* Configure USART4 Rx as input floating */
  GPIO_InitStructure.GPIO_Pin = USART4_RxPin;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(USART4_GPIO, &GPIO_InitStructure);
  
  /* Configure USART4 Tx as alternate function push-pull */
  GPIO_InitStructure.GPIO_Pin = USART4_TxPin;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(USART4_GPIO, &GPIO_InitStructure);
	
	/* Configure PC9 as alternate function push-pull */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	GPIO_WriteBit(GPIOC,GPIO_Pin_9,Bit_RESET);
	
	/* Configure PC1 as  function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
  GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	/* Configure PB12 as alternate function push-pull */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12  ; 
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
	
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8  ; 
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	GPIO_WriteBit(GPIOB,GPIO_Pin_12,Bit_RESET);
	GPIO_WriteBit(GPIOB,GPIO_Pin_8,Bit_RESET);
}

/**
  * @brief  Configures the DMA.
  * @param  None
  * @retval None
  */
void DMA_Configuration(void)
{
  DMA_InitTypeDef DMA_InitStructure;

  /* USART4 TX DMA1 Channel (triggered by USART4 Tx event) Config */
  DMA_DeInit(USART4_Tx_DMA_Channel);  
  DMA_InitStructure.DMA_PeripheralBaseAddr = USART4_DR_Base;
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)UartTxBuffer;
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
  DMA_InitStructure.DMA_BufferSize = 0;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
  DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
  DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
  DMA_Init(USART4_Tx_DMA_Channel, &DMA_InitStructure);
}


void UART_DMA_TX(u32 Address,u16 count)
{
	DMA_InitTypeDef DMA_InitStructure;

  /* USART4 TX DMA1 Channel (triggered by USART4 Tx event) Config */
  DMA_InitStructure.DMA_PeripheralBaseAddr = USART4_DR_Base;
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)Address;
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
  DMA_InitStructure.DMA_BufferSize = count;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
  DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
  DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	
	/* Check if DMA1 channel3 transfer is finished */
	while(USART4_Tx_DMA_Channel->CNDTR!=0);
	DMA_Cmd(USART4_Tx_DMA_Channel, DISABLE);
	DMA_ClearFlag(DMA2_FLAG_TC5);
  DMA_Init(USART4_Tx_DMA_Channel, &DMA_InitStructure);
	DMA_Cmd(USART4_Tx_DMA_Channel, ENABLE);
	
//  /* Disable the selected DMAy Channelx */
//  USART4_Tx_DMA_Channel->CCR &= (uint16_t)(~DMA_CCR1_EN);
//  /* Clear DMA1 channel4 transfer complete flag bit */
//  DMA2->IFCR = USART4_Tx_DMA_FLAG;
//  /* Write to DMAy Channelx CMAR */
//  USART4_Tx_DMA_Channel->CMAR = Address;
//  /* Write to DMAy Channelx CNDTR */
//  USART4_Tx_DMA_Channel->CNDTR = count;  
//  /* Enable DMA1 channel3 */
//  USART4_Tx_DMA_Channel->CCR |= DMA_CCR1_EN;
}

void UART2_DMA_TX(u32 Address,u16 count)
{
	while(DMA1_Channel7->CNDTR!=0);
  /* Disable the selected DMAy Channelx */
  DMA1_Channel7->CCR &= (uint16_t)(~DMA_CCR1_EN);
  /* Clear DMA1 channel4 transfer complete flag bit */
  DMA1->IFCR = DMA1_FLAG_TC7;
  /* Write to DMAy Channelx CMAR */
  DMA1_Channel7->CMAR = Address;
  /* Write to DMAy Channelx CNDTR */
  DMA1_Channel7->CNDTR = count;  
  /* Enable DMA1 channel3 */
  DMA1_Channel7->CCR |= DMA_CCR1_EN;
}

/**
  * @brief  Compares two buffers.UART2_DMA_TX((u32)
  * @param  pBuffer1, pBuffer2: buffers to be compared.
  * @param  BufferLength: buffer's length
  * @retval PASSED: pBuffer1 identical to pBuffer2
  *         FAILED: pBuffer1 differs from pBuffer2
  */
TestStatus Buffercmp(uint8_t* pBuffer1, uint8_t* pBuffer2, uint16_t BufferLength)
{
  while(BufferLength--)
  {
    if(*pBuffer1 != *pBuffer2)
    {
      return FAILED;
    }

    pBuffer1++;
    pBuffer2++;
  }

  return PASSED;
}

void uart_init(u32 bound){
  //GPIO 
  GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1|RCC_APB2Periph_GPIOA, ENABLE);	//ENABLE USART1,GPIOA clock
  
	//USART1_TX   GPIOA.9
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; //PA.9
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	
  GPIO_Init(GPIOA, &GPIO_InitStructure);//init GPIOA.9
   
  //USART1_RX	  GPIOA.10 init
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;//PA10
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOA, &GPIO_InitStructure);//init GPIOA.10  

  //Usart1 NVIC 
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3 ;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
  
  //USART
	USART_InitStructure.USART_BaudRate = bound;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

  USART_Init(USART1, &USART_InitStructure);
  USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
  USART_Cmd(USART1, ENABLE);

}

void My_Usart1_Send(char *string,uint16_t length)
{
	Tx1CounterSize = length;
	memcpy(Tx1buf,string,Tx1CounterSize);
	Tx1count=0;
	USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
}

void My_Usart2_Send(char *string,uint16_t length)
{
	Tx2CounterSize = length;
	memcpy(Tx2buf,string,Tx2CounterSize);
	Tx2count=0;
	USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
}

/* USART1 init function */
static void MX_USART1_UART_Init(void)
{
  //GPIO 
  GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1|RCC_APB2Periph_GPIOA, ENABLE);	//ENABLE USART1,GPIOA clock
  
	//USART1_TX   GPIOA.9
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; //PA.9
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	
  GPIO_Init(GPIOA, &GPIO_InitStructure);//init GPIOA.9
   
  //USART1_RX	  GPIOA.10 init
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;//PA10
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOA, &GPIO_InitStructure);//init GPIOA.10  

  //Usart1 NVIC 
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3 ;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
  
  //USART
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

  USART_Init(USART1, &USART_InitStructure);
  USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
  USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
  USART_Cmd(USART1, ENABLE);
}

#define USART2_DR_Base           0x40004404
/* USART2 init function */
static void MX_USART2_UART_Init(uint32_t baud)
{
  //GPIO 
	DMA_InitTypeDef DMA_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);	//ENABLE USART2
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);	//ENABLE USART1,GPIOA clock
  
	
  /* USART4 TX DMA1 Channel (triggered by USART4 Tx event) Config */
  DMA_DeInit(DMA1_Channel7);  
  DMA_InitStructure.DMA_PeripheralBaseAddr = USART2_DR_Base;
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)Uart2TxBuffer;
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
  DMA_InitStructure.DMA_BufferSize = 0;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
  DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
  DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
  DMA_Init(DMA1_Channel7, &DMA_InitStructure);
  
	//USART2_TX   GPIOA.2
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2; //PA.2
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	
  GPIO_Init(GPIOA, &GPIO_InitStructure);//init GPIOA.2
   
  //USART2_RX	  GPIOA.3 init
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;//PA3
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOA, &GPIO_InitStructure);//init GPIOA.3  

  //Usart1 NVIC 
  NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=3 ;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
  
  //USART
	USART_InitStructure.USART_BaudRate = baud;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

  USART_Init(USART2, &USART_InitStructure);
  //USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
  USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	
	/* Enable USART4 DMA Rx and TX request */
  USART_DMACmd(USART2, USART_DMAReq_Tx, ENABLE);
  /* Enable USART4 TX DMA1 Channel */
  DMA_Cmd(DMA1_Channel7, DISABLE);
	
  USART_Cmd(USART2, ENABLE);
}

void USART1_IRQHandler(void)
{
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
	{
		//Receive interrupt
		Rx1buf[Rx1count] = USART_ReceiveData(USART1);				//Receive data
		if((Rx1buf[Rx1count-1]==0x0D)&&(Rx1buf[Rx1count]==0x0A))
		{
			//end byte is 0D(\r) 0A(\n)
			//My_Usart2_Send(Rx1buf,strlen((char *)Rx1buf));	//transmit data to Uart2
			Rx1count= 0;															//clear Receive count
		}
		else
		{
			Rx1count++;																
		}
	}
	
	if(USART_GetITStatus(USART1, USART_IT_TXE) != RESET)
	{
		//transmit  interrupt
		USART_SendData(USART1, Tx1buf[Tx1count++]);
    if(Tx1count >= Tx1CounterSize)//transmit end
    {
			memset(Tx1buf,0x00,Tx1CounterSize);
			Tx1CounterSize = 0;
      USART_ITConfig(USART1, USART_IT_TXE, DISABLE); //close transmit  interrupt
    } 
	}
}

void USART2_IRQHandler(void)
{
	if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
	{
		//Receive interrupt
		
		Rx2buf[Rx2count] = USART_ReceiveData(USART2);				//Receive data
		if(Rx2count>Rxbufsize)
		{
			memset(Rx2buf,0x00,strlen((char *)Rx2buf));
			Rx2count = 0;																//Receive over for 200byte clear Receive count
		}
		else
		{
			if((Rx2buf[Rx2count-1]==0x0D)&&(Rx2buf[Rx2count]==0x0A))
			{
				//end byte is 0D(\r) 0A(\n)
				Rx2count= 0;															//clear Receive count
			}
			else
			{
				Rx2count++;																
			}
		}
		
	}
	
	if(USART_GetITStatus(USART2, USART_IT_TXE) != RESET)
	{
		//transmit  interrupt
		USART_SendData(USART2, Tx2buf[Tx2count++]);
    if(Tx2count >= Tx2CounterSize)//transmit end
    {
			memset(Tx2buf,0x00,Tx2CounterSize);
			Tx2CounterSize = 0;
      USART_ITConfig(USART2, USART_IT_TXE, DISABLE); //close transmit  interrupt
    } 
	}
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}

#endif

/**
  * @}
  */ 

/**
  * @}
  */ 

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
