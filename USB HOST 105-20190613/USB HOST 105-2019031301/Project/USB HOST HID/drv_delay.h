/**
  ******************************************************************************
  * @author  澤耀科技 ASHINING
  * @version V3.0
  * @date    2016-10-08
  * @brief   DELAY配置H文件
  ******************************************************************************
  * @attention
  *
  * 官網	:	http://www.ashining.com
  * 淘寶	:	https://shop105912646.taobao.com
  * 阿里巴巴:	https://cdzeyao.1688.com
  ******************************************************************************
  */


#ifndef __DRV_DELAY_H__
#define __DRV_DELAY_H__


#include "stm32f10x_rcc.h"
#include "stm32f10x_tim.h"


/** 採用定時器做精確延時 */
//延時硬件定義
#define DELAY_TIME_BASE					TIM2
#define DELAY_TIME_BASE_CLK				RCC_APB1Periph_TIM2


void drv_delay_init( void );
void drv_delay_us( uint16_t Us );
void drv_delay_ms( uint8_t Ms );
void drv_delay_500Ms( uint8_t Ms_500 );
void drv_delay_free( uint32_t Delay_Time );

void delay(vu32 nCount);
#endif

